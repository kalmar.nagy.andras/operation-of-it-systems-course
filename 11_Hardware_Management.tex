\documentclass[12pt]{report}

\newcommand\lessontitle{Lecture 11 -- Hardware Management}
\newcommand\lessondate{April 22, 2021}

\usepackage[a4paper, landscape, margin=2cm]{geometry}
\usepackage{fontspec}
\usepackage{microtype}
\usepackage{fancyhdr}
\usepackage{sectsty}
\usepackage{graphicx}
\usepackage{flafter}
\usepackage{hyperref}
\usepackage{afterpage}
\usepackage{wrapfig}
\usepackage{xcolor}
\usepackage{tabularx}
\usepackage{amssymb}
\usepackage{listings}

\graphicspath{ {./images/} }

\setmainfont{CallunaSans-Regular.otf}[
  BoldFont = CallunaSans-Bold.otf ]
\setsansfont{MuseoSans-500.otf}[
  BoldFont = MuseoSans-900.otf,
  ItalicFont = MuseoSans-500Italic.otf ]
\setmonofont{UbuntuMono-R.ttf}

\pagestyle{fancy}
\fancyhf{}
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0.5pt}
\lhead{\textsf{Operation of IT Systems -- ENPTIA2401-PRACT}}
\rhead{\textsf{\lessontitle}}
\rfoot{\textsf{Page \thepage}}
\lfoot{\textsf{\lessondate}}

\setcounter{secnumdepth}{0}

\setlength{\parindent}{0pt}

\sectionfont{\clearpage\Huge\sffamily}
\subsectionfont{\sffamily}

\newcommand\cli[1]{\colorbox{gray!30}{\texttt{#1}}}

\hypersetup{
    colorlinks=true,
    linkcolor=black,
    urlcolor=black,
}
\urlstyle{same}

\lstdefinestyle{mystyle}{
    basicstyle=\ttfamily,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=none,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}

\lstset{style=mystyle}

\begin{document}

\begin{titlepage}
{\Huge \bfseries \textsf{Operation of IT Systems -- ENPTIA2401-PRACT} \par}
\vspace{1cm}
{\Large \bfseries \textsf{\lessontitle} \par}
\vspace{1cm}
{\Large \textsf{\lessondate} \par}
\vfill
{\large \textsf{András Kalmár-Nagy (\href{mailto:andras.kalmar-nagy@t-systems.com}{andras.kalmar-nagy@t-systems.com})}}
\end{titlepage}

\section{Lifecycle Management}

IT Service Management is all about cycles. There are three distinct cycles, which have an impact on hardware. The service lifecycle, where a service is created to fulfil a demand, the service is provided by hardware and software assets, and then the hardware and software is continuously improved to provide the service. At the end of the lifecycle, the hardware is decommissioned and responsibly disposed of.\\

The length of these cycles is determined by the lifetime of the services they provide, the support that comes from vendors, and the development cadence of the software.\\

A service is usually sold as a product, and most successful products and services exist for at least five to ten years. Hardware is usually supported for 3-5 years by most vendors, and software is developed even faster.\\

\section{Service Lifecycle}

\subsection{Design \& Development}
Most services are created to fulfil a need in the market, to meet a demand. If a company sees an opportunity, they start designing a service to meet it. The technical solution to a problem is one part of it, but the service also needs to have a business model. If you are losing money on a service, then the more people use it, the more money it ends up costing.\\

Large projects can take up to, or more then a year to design and develop into a working, ready-for-sale product. Startups can develop products and services faster, but they may lack the resilience of larger companies.

\subsection{Productive Lifespan}
Once a service is created and is ready for sale, it can be sold to end-users. One challenge when something is starting out, is the need to scale the service with the rate of adoption. If the service is scaled up too fast, then the capacity just sits there consuming money without users to pay for it. If the scaling is too slow, then customers may become frustrated and may move to a competitor who can provide a smoother service. Mistakes made during the design phase may make scaling a service very problematic, and some systems may not scale well. If the bottlenecks that inhibit scaling are not eliminated, they may prevent the service from gaining traction, and the customers will quickly switch to another service.\\

Services evolve naturally over their lifespan, and some companies may discover completely new markets and services by listening to their customers. One such story is the birth of Amazon Web Services, which started as a platform to support the web-based marketplace business of Amazon, but has become the source of 60\% of their operating profit (12\% of revenue).

\subsection{Rundown}

As markets evolve, and the needs of customers change, some services may not be able to evolve with demand. These services slowly lose their customers and become unprofitable. Such services are discontinued, so that the company can allocate the resources to other, more profitable services and products. Sometimes a service is deprecated in favour of a better or more profitable service that the company has.\\

It is also possible for a service to evolve with the changing needs of customers, and these services may survive for a long time, often undergoing several transformations.

\section{Hardware Lifecycle}

Hardware is used to provide the technical basis of the services that a company provides. There are two sides to hardware that a company uses. There is hardware that is used in ``production'', this will be the servers and other supporting devices in a datacentre setting; and then there are the devices with which people in the company work, like laptops, monitors, printers and phones.\\

Average lifespans for hardware:
\begin{itemize}
\item \textbf{Laptops} -- 3-5 years with regular patching and updating.
\item \textbf{Monitors} -- 10-15 years with continuous use (8 hours/day).
\item \textbf{Laptop docking station} -- These are simple pieces of hardware, they may last 5-10 years, but are dependent on the laptops.
\item \textbf{Mice and Keyboards} -- 3-4 years, depending on quality and usage.
\item \textbf{Printers} -- 5-8 years, depending on vendor support, availability of consumables.
\item \textbf{Hard drives} -- usually at least 4 years, but this depends on usage patterns and can vary strongly. Always have tested backups!
\item \textbf{Servers} -- 3-5 years, depending on vendor support. The hardware itself can last a lot longer, and there is a large second-hand market for used server-grade hardware.
\item \textbf{Networking equipment} -- 7-10 years, depending on vendor support.
\item \textbf{Power components, UPSes} -- 4-6 years, although batteries may have shorter lifespans (around 3 years).
\end{itemize}

\subsection{Procurement}
Procurement deals with the capacity planning-, and management of hardware devices to fulfil the company's needs. Planning involves finding the right balance in capacity, sourcing the hardware with the right features and planning the life of the devices.\\

Procurement also involves the negotiating with vendors. Large companies can buy devices in bulk (a thousand laptops at once, for example), and this may mean better prices for the company. Vendors may also propose bundles or offer additional services for large deals.\\

A successful procurement cycle ends with the hardware being delivered to the company, ready for the next step.

\subsection{Deployment}
The procured hardware is deployed, so that it can be put to work.\\

Deployment involves registering the devices in the inventory of the company, making sure the devices are in working order, and physically installing the devices in their place. The devices are then configured to work with the rest of the company's infrastructure, and connected to monitoring and alerting systems if they support remote management.

\subsection{Management}
A device needs to be managed during its productive life. Vendors publish firmware and software updates that need to be installed on the devices, some devices might be moved to other workloads and may need to be reconfigured for their new role. Devices also need physical maintenance, if a unit becomes faulty, then it needs to be repaired or replaced.

\subsection{Decommission}
when a device reaches the end of its life, it gets decommissioned and either sold or disposed. It is important to have a proper process in place for the decommissioning of devices as any data accidentally left on devices can mean a security breach, with heavy fines for sensitive data. There have been several cases of people buying used hard-drives and finding sensitive data on them. Particularly sensitive devices can also be destroyed to eliminate the chance of a data leak.

\subsection{Disposition}
The decommissioned devices that are not sold, are destroyed. There are regulations on how hardware can be disposed, and several companies specialise in the proper disposal and recycling of IT hardware, as circuit boards and other components in IT hardware can contain toxic or hazardous substances.

\section{Software Lifecycle}
Since software is not physical, it can have a far shorter lifecycle, as it isn't tied to a physical object.

\subsection{Design \& Development}
How a service is rendered, depends in large part on the software, and what features it provides. Every piece of software must be developed to meet the needs of the service that it implements. There are several methodologies for developing software, the two most common are ``waterfall'' and ``agile''. Waterfall is a rigid product oriented approach which is easier to manage, but isn't as responsive to changing specifications and customer demands. It is usually typical of large companies with large management structures. Agile is a customer oriented development process, and is based on quickly iterating and testing small features and delivering these features to the customer, so that feedback is also fast. An agile process is capable of reacting very quickly to customer demands. Startups usually prefer to use agile methods because they don't involve a large upfront investment of time and effort.

\subsection{Release process}
When a piece of software is ready, it is usually tested before being released into production. Testing is a quality assurance measure, and makes sure that the software behaves as it should, and can also guarantee that old errors that have been fixed in previous versions, don't reappear (regression testing). Once a piece of software passes the testing stage, it is deployed to production. This can happen automatically in modern systems, or by hand in classic IT models. The automated testing and deployment of software is often called Continuous Integration/Continuous Deployment (CI/CD). CI/CD allows developers to deploy new releases very quickly, even several times a day. Release cycles in classic IT are usually measured in months.

\subsection{Operation}
Deployed software is operated by system-,  or application administrators. ``AppAdmins'' make sure that the software continues working as it should, and handle cases when it doesn't. If a problem is discovered in a piece of software, then a problem ticket is issued, which lets the developers know what to correct in the next release of the software.\\

Sometimes a fix for a problem doesn't warrant a complete new release of the software, and changes are applied as updates, or patches. Patch management is a large part of classic IT operation, and it also involves the updating of the operating systems for the hardware, not just the end-user/middleware software. Patching is both time consuming and error prone, and automating it has several benefits.

\section{Outsourcing}

Outsourcing is the practice of buying or renting services and hardware from specialised providers, if the company doesn't want to invest time and money in maintaining an in-house service. With all outsourcing, the trade-off is lower investment cost and more flexibility for loss of control and higher overall operating cost.

\subsection{Service Centres}
Company functions, such as accounting, HR and customer service can be rented from providers. These providers may operate in areas where labour is cheaper, or can offer price benefits because of economies of scale. If such services are used, care must be taken to ensure that end-customers receive quality service from these third-party providers.

\subsection{Infrastructure providers}
Infrastructure providers offer flexibility and eliminate the need for a large upfront investment for companies that are just starting out. For small startup, the price and management of hardware can be prohibitively expensive, but cloud platforms enable these companies to develop and scale quickly. Large companies may rent space in datacentres for their own hardware, while leaving the management of the datacentre to the provider.

\subsection{Software Development}
Some companies may opt to outsource software development to other companies. This usually involves creating a discrete piece of software as part of a project. There are a lot of small software development houses that specialise in a specific field and also offer consulting.

\subsection{IT service providers}
There are companies that provide an end-to-end IT service that involves customer service,- infrastructure-, and development services with a pick-and-mix portfolio. These companies are able to offer integrated services and economies of scale to customers.

\section{Exam questions}
\begin{itemize}
  \item \textbf{What benefits does outsourcing provide?} -- Flexibility and lower investment costs.
  \item \textbf{What is the lifespan of server hardware?} -- 3-5 years depending on vendor support. Vendors stop supporting hardware after this time, which means that they don't fix faults or issue new software or security updates.
  \item \textbf{What are the benefits of an agile software development process} -- Customer focus, fast development of features.
  \item \textbf{What does CI/CD stand for?} -- Continuous Integration/Continuous Deployment
\end{itemize}

\end{document}

\begin{wrapfigure}{r}{0.5\textwidth}
  \begin{center}
    \includegraphics[width=0.48\textwidth]{IBM_350_RAMAC}
  \end{center}
  \caption{IBM RAMAC 350 disk mechanism}
\end{wrapfigure}
