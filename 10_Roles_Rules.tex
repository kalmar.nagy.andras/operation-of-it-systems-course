\documentclass[12pt]{report}

\newcommand\lessontitle{Lecture 10 -- Roles, Rules on System Administrator Level}
\newcommand\lessondate{April 15, 2021}

\usepackage[a4paper, landscape, margin=2cm]{geometry}
\usepackage{fontspec}
\usepackage{microtype}
\usepackage{fancyhdr}
\usepackage{sectsty}
\usepackage{graphicx}
\usepackage{flafter}
\usepackage{hyperref}
\usepackage{afterpage}
\usepackage{wrapfig}
\usepackage{xcolor}
\usepackage{tabularx}
\usepackage{amssymb}
\usepackage{listings}

\graphicspath{ {./images/} }

\setmainfont{CallunaSans-Regular.otf}[
  BoldFont = CallunaSans-Bold.otf ]
\setsansfont{MuseoSans-500.otf}[
  BoldFont = MuseoSans-900.otf,
  ItalicFont = MuseoSans-500Italic.otf ]
\setmonofont{UbuntuMono-R.ttf}

\pagestyle{fancy}
\fancyhf{}
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0.5pt}
\lhead{\textsf{Operation of IT Systems -- ENPTIA2401-PRACT}}
\rhead{\textsf{\lessontitle}}
\rfoot{\textsf{Page \thepage}}
\lfoot{\textsf{\lessondate}}

\setcounter{secnumdepth}{0}

\setlength{\parindent}{0pt}

\sectionfont{\clearpage\Huge\sffamily}
\subsectionfont{\sffamily}

\newcommand\cli[1]{\colorbox{gray!30}{\texttt{#1}}}

\hypersetup{
    colorlinks=true,
    linkcolor=black,
    urlcolor=black,
}
\urlstyle{same}

\lstdefinestyle{mystyle}{
    basicstyle=\ttfamily,
    breakatwhitespace=false,         
    breaklines=true,                 
    captionpos=b,                    
    keepspaces=true,                 
    numbers=none,                    
    numbersep=5pt,                  
    showspaces=false,                
    showstringspaces=false,
    showtabs=false,                  
    tabsize=2
}

\lstset{style=mystyle}

\begin{document}

\begin{titlepage}
{\Huge \bfseries \textsf{Operation of IT Systems -- ENPTIA2401-PRACT} \par}
\vspace{1cm}
{\Large \bfseries \textsf{\lessontitle} \par}
\vspace{1cm}
{\Large \textsf{\lessondate} \par}
\vfill
{\large \textsf{András Kalmár-Nagy (\href{mailto:andras.kalmar-nagy@t-systems.com}{andras.kalmar-nagy@t-systems.com})}}
\end{titlepage}

\section{Local User and Group Permissions}

All modern multi-user OSes support the concept of user and group ownership. Files, directories and resources all have permissions based on user and group IDs.\\

Each file has metadata associated with it, that contains which user and group has ownership of the file, what rights the user, the group and others have to the file, and other additional attributes the file might have.\\
\begin{lstlisting}[frame=single,caption=Linux file attributes]
[andras@squishy ~]$ stat test.txt
  File: ‘test.txt’
  Size: 0               Blocks: 0          IO Block: 4096   regular empty file
Device: fd03h/64771d    Inode: 10698       Links: 1
Access: (0640/-rw-r-----)  Uid: (400002/andras)   Gid: (400000/opm)
Access: 2021-04-13 22:19:19.076499408 +0200
Modify: 2021-04-13 22:19:19.076499408 +0200
Change: 2021-04-13 22:19:19.076499408 +0200
 Birth: -
\end{lstlisting}
In the above example, the file is owned by the user \texttt{andras} and the group \texttt{opm}. The owner has read and write rights, the group can only read the file, and all others have no rights on the file. The permissions are shown in octal notation (\texttt{0640}), and also with letters: \texttt{-rw-r-----}. The four octal digits describe four sets of rights. The first digit shows whether any of the special bits are set (discussed later), the second digit shows the permissions of the owner of the file, it is a sum of 4=read, 2=write, 1=execute. So, for example, if the user has read and write permissions, the value is 4+2. The third and fourth digits denote the permissions of the group and `others', others being users who are not the owners, and who are not in the group of the file. The letter notation shows each permission group as three letters or dashes, and the first letter is used to denote the special bits. The three positions are \texttt{rwx}, where the corresponding letter means that the permission is set, while a dash denotes that it is absent.\\

The execute permission means different things for files and directories. If the executable permission is set on a file, the shell will try to execute the file if it is invoked. For a directory, the executable bit means, that the directory can be accessed by the respective group. If a directory does not have execute permissions, then you won't able to cd into it.

\subsection{Special Bits}

\begin{wrapfigure}{r}{0.4\textwidth}
  \begin{center}
    \includegraphics[width=0.35\textwidth]{windows_permissions}
  \end{center}
  \caption{Windows permissions dialog}
\end{wrapfigure}

The first octal digit refers to the special bits that can be set on a file. These are the setuid (4), the setgid (2) and the sticky (1) bit. If the setuid bit is set, and the file is executable, then the file will run with it's owners rights. This is useful if you want to let others run scripts with your user (this could also be dangerous, if used irresponsibly). The setgid bit is used with directories. If a directory has its setguid bit set, then all the files created in that directory will automatically have their group set to the group of the directory. And finally, the sticky-bit, when applied to a directory, ensures that the files inside can only be modified by the owner, the directory's owner or root.

\subsection{Access Control Lists}

Some filesystems support extended rights on files, these are stored as Access Control Lists (ACLs). ACLs allow per-user permissions for files and directories. In an ACL, individual users are listed with their specific rights. This is useful if two users, who are not part of the same group need access to a file, or users within a group need different permissions.

\begin{lstlisting}[frame=single]
# getfacl /tmp/test
# file: test
# owner: root
# group: root
user::rw-
user:john:rw-
user:sam:rwx
group::r--
mask::rwx
other:---
\end{lstlisting}
The above output shows the users sam and john having different rights to the same file.

\section{Role Based Access Control}

File based permissions make sense from the view of the filesystem, but are hard to maintain and synchronise if you have a lot of users and machines. Role Based Access Control (RBAC) groups users according to roles, with rights assigned to the roles, not the individual users. Any user assigned to a role will automatically inherit the rights of the role.\\

For example, you can have roles like ``administrator'', ``manager'' or ``accounting''. Users can be assigned to the appropriate roles, and they will automatically have the right permissions set by the system. If a user changes roles, or leaves the company, their rights can be simply altered by deleting their assignment from the roles they had.\\

This is particularly important in large companies where there is a lot of fluctuation in the workforce. When a new colleague is onboarded, they are given the roles appropriate for their jobs, and then they will automatically have the correct access rights and permissions. When they leave the role, their assignment is revoked, and their access rights and permissions are automatically adjusted. This is an important security and compliance measure, as unused accounts on computers can be a security risk. For industries, where access to data is tightly controlled (banks, healthcare, etc.), users with outdated or too wide permissions are also a compliance and security risk.\\

RBAC is also employed on cloud platforms, like Amazon Web Services and Microsoft Azure, where access rights for individuals and also robot accounts can be assembled from pre-built role templates. RBAC is also used in Kubernetes, a container orchestration platform.

\section{Active Directory}

Active Directory, commonly abbreviated as AD, is a directory service that was developed by Microsoft for Windows. It is included in Windows Server operating systems. AD was first developed to provide domain management (users and resources), but other features have since been integrated into it. AD is used to manage large deployments of Windows machines, and allows administrators to manage users; control access rights to machines, services and files; software updates, and other identity and security related functions.

\subsection{Active Directory Services}
Active Directory Services consist of multiple directory services. The best known is Active Directory Domain Services, commonly abbreviated as AD DS or simply AD.

\subsection{Domain Services}
Active Directory Domain Services (AD DS) is the foundation stone of every Windows domain network. It stores information about members of the domain, including devices and users, verifies their credentials and defines their access rights. The server running this service is called a domain controller. A domain controller is contacted when a user logs into a device, accesses another device across the network, or runs a line-of-business Metro-style app sideloaded into a device.\\

Other Active Directory services (excluding LDS, as described below) as well as most of Microsoft server technologies rely on or use Domain Services; examples include Group Policy, Encrypting File System, BitLocker, Domain Name Services, Remote Desktop Services, Exchange Server and SharePoint Server.\\

The self-managed AD DS must not be confused with managed Azure AD DS, which is a cloud product.

\subsection{Lightweight Directory Services}
Active Directory Lightweight Directory Services (AD LDS), formerly known as Active Directory Application Mode (ADAM), is an implementation of LDAP protocol for AD DS. AD LDS runs as a service on Windows Server. AD LDS shares the code base with AD DS and provides the same functionality, including an identical API, but does not require the creation of domains or domain controllers. It provides a Data Store for storage of directory data and a Directory Service with an LDAP Directory Service Interface. Unlike AD DS, however, multiple AD LDS instances can run on the same server.

\clearpage
\subsection{Certificate Services}
Active Directory Certificate Services (AD CS) establishes an on-premises public key infrastructure. It can create, validate and revoke public key certificates for internal uses of an organization. These certificates can be used to encrypt files (when used with Encrypting File System), emails (per S/MIME standard), and network traffic (when used by virtual private networks, Transport Layer Security protocol or IPSec protocol).\\

AD CS predates Windows Server 2008, initially, its name was simply Certificate Services.\\

AD CS requires an AD DS infrastructure.

\subsection{Federation Services}
Active Directory Federation Services (AD FS) is a single sign-on service. With an AD FS infrastructure in place, users may use several web-based services (e.g. internet forum, blog, online shopping, webmail) or network resources using only one set of credentials stored at a central location, as opposed to having to be granted a dedicated set of credentials for each service. AD FS's purpose is an extension of that of AD DS: The latter enables users to authenticate with and use the devices that are part of the same network, using one set of credentials. The former enables them to use the same set of credentials in a different network.As the name suggests, AD FS works based on the concept of federated identity.\\

AD FS requires an AD DS infrastructure, although its federation partner may not.

\subsection{Rights Management Services}
Active Directory Rights Management Services (AD RMS, known as Rights Management Services or RMS before Windows Server 2008) is a server software for information rights management shipped with Windows Server. It uses encryption and a form of selective functionality denial for limiting access to documents such as corporate e-mails, Microsoft Word documents, and web pages, and the operations authorized users can perform on them.

\section{Other Access Models}

These models are alternatives, that offer a different take on security and access management. I collected them here, because they demonstrate different ways of thinking about rights and access.

\subsection{Zero Trust Security Model}

In ``classic'' IT, security is usually enforced at the perimeter of the secure network, and machines inside the same network can usually trust each other. This is possible if you have tight control over the hardware, the machines and the software on them.\\
In a cloud based infrastructure, This degree of trust is not possible, because of all the abstraction layers and because the hardware is not managed by the end user. In this situation, services and machines need to be able to authenticate themselves to other machines and services on the network. This allows tighter security, because all communication and participants are verified, before they are trusted, and trust is not just simply assumed based on being on the same network.

\subsection{OAuth}

OAuth is a standard for access delegation. Access delegation allows a user to grant websites or applications access to their information on other websites but without giving them the passwords. For example, I can allow a website to access my data in Google Calendar by issuing an OAuth token. The token is like a long password that identifies the service that wants to access my data, to Google. Google then asks me, if I want to allow that particular service to access the data it wants to. If I tell Google to allow the access, then the third-party service will be able to access that specific data, to which it has access, and nothing more.

\subsection{Open Policy Agent}

The Open Policy Agent (OPA) is a service which can grant or deny access to resources based on a centrally managed policy. For example, I can allow a certain user to only access a machine during office hours. If the user tries to log in the machine, the server asks the OPA, whether the user can be allowed to log in. The OPA then checks the central policy, and if the user is allowed to log in, then the server is instructed to allow the user. If the policy denies the login, then the server is instructed by the OPA to stop the connection attempt. There are lot of things that can be allowed or denied based on OPA policies. For example, you can control what sort of software packages can be installed on a machine, what sort of programs may run, when and who is allowed to log into a machine. The OPA can also be used on cloud-, and container platforms. It can control what can be deployed on the infrastructure, and what sort of rules that infrastructure must adhere to.

\section{Interesting Links}
\begin{itemize}
\item \href{https://developers.google.com/identity/protocols/oauth2}{Google documentation on accessing its services with OAuth}
\item \href{https://www.openpolicyagent.org/}{Open Policy Agent website}
\item \href{https://cloud.google.com/security/beyondprod}{Zero Trust Architecture at Google explained}
\end{itemize}

\section{Exam questions}
\begin{itemize}
  \item \textbf{What do the permissions \texttt{-rw-rw-r--} mean for a file?} -- Read + Write rights for the user, Read + Write rights for the group and only Read rights for others.
  \item \textbf{Why is important to have file ownership and access rights?} -- In a multi-user system, file ownership and access rights provide isolation between users and protect data from unauthorised use.
  \item \textbf{What does RBAC stand for?} -- Role Based Access Control
  \item \textbf{What is Active Directory?} -- A directory service, developed by Microsoft. It can be used to manage a large number of Windows based machines, access rights and other identity and security related resources.
\end{itemize}

\end{document}

\begin{wrapfigure}{r}{0.5\textwidth}
  \begin{center}
    \includegraphics[width=0.48\textwidth]{IBM_350_RAMAC}
  \end{center}
  \caption{IBM RAMAC 350 disk mechanism}
\end{wrapfigure}
