\documentclass[12pt]{report}

\newcommand\lessontitle{Lecture 3 -- Commands and GUI}
\newcommand\lessondate{February 18, 2021}

\usepackage[a4paper, landscape, margin=2cm]{geometry}
\usepackage{fontspec}
\usepackage{microtype}
\usepackage{fancyhdr}
\usepackage{sectsty}
\usepackage{graphicx}
\usepackage{flafter}
\usepackage{hyperref}
\usepackage{afterpage}
\usepackage{wrapfig}
\usepackage{xcolor}
\usepackage{tabularx}

\graphicspath{ {./images/} }

\setmainfont{CallunaSans-Regular.otf}[
  BoldFont = CallunaSans-Bold.otf ]
\setsansfont{MuseoSans-500.otf}[
  BoldFont = MuseoSans-900.otf,
  ItalicFont = MuseoSans-500Italic.otf ]
\setmonofont{UbuntuMono-R.ttf}

\pagestyle{fancy}
\fancyhf{}
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0.5pt}
\lhead{\textsf{Operation of IT Systems -- ENPTIA2401-PRACT}}
\rhead{\textsf{\lessontitle}}
\rfoot{\textsf{Page \thepage}}
\lfoot{\textsf{\lessondate}}

\setcounter{secnumdepth}{0}

\setlength{\parindent}{0pt}

\sectionfont{\clearpage\Huge\sffamily}
\subsectionfont{\sffamily}

\newcommand\cli[1]{\colorbox{gray!30}{\texttt{#1}}}

\hypersetup{
    colorlinks=true,
    linkcolor=black,
    urlcolor=black,
}
\urlstyle{same}

\begin{document}

\begin{titlepage}
{\Huge \bfseries \textsf{Operation of IT Systems -- ENPTIA2401-PRACT} \par}
\vspace{1cm}
{\Large \bfseries \textsf{\lessontitle} \par}
\vspace{1cm}
{\Large \textsf{\lessondate} \par}
\vfill
{\large \textsf{András Kalmár-Nagy (\href{mailto:andras.kalmar-nagy@t-systems.com}{andras.kalmar-nagy@t-systems.com})}}
\end{titlepage}

\section{The GUI}

\begin{wrapfigure}{r}{0.5\textwidth}
  \begin{center}
    \includegraphics[width=0.48\textwidth]{mouse}
  \end{center}
  \caption{The first mouse, invented by Douglas Engelbart in 1963}
\end{wrapfigure}


GUI is an abbreviation of graphical user interface. Graphical user interfaces use graphics to represent programs running on a computer. A pointing device or a touch screen is usually employed in addition to a keyboard, to navigate the interface.\\

GUIs were created to make computers easier to use for novice or inexperienced users and counter the perceived steep learning curve of command line interfaces. Graphical interfaces are popular, because they allow a richer representation of content than command line interfaces. Some software would not be possible without a graphical user interface, like photo- and video editing applications.\\

The first commercial system with a GUI was the  Xerox Star 8010, which was introduced in 1981. Apple released the Apple Lisa in 1984, which already has the concept of a menu bar, and window controls, like we have on modern systems.\\

For a long time, the most popular primarily GUI-based OS was Microsoft Windows, until it was overtaken by Android in April of 2017.

\clearpage 

\begin{figure}[t]
\includegraphics[width=\textwidth]{linux_desktop}
\caption{A modern Linux desktop from the r/unixporn subreddit.}
\end{figure}

\section{Command Line Interfaces}

Command line interfaces are text-based, where the user has to type in instructions, or commands, for the computer to perform. Text interfaces are popular because they have very low resource overheads, and allow very efficient work if the user is proficient.\\

Command line interfaces lend themselves well to automation through scripting. Scripts are collected commands which are executed by a command interpreter, or shell. A lot of repetitive tasks can be automated using scripting, and although the commands are fairly simple, they can be chained together to tackle complex tasks.\\

\subsection{Unix-like systems}

Modern Unix-like systems rely heavily on their command line interfaces. The most prominent shell is \texttt{bash}, which stands for Bourne Again SHell. Bash has a lot of features that simplify its use, like variables, globbing, piping and control structures. It is a programming language in itself.\\

The Unix philosophy is that commands should do one simple thing, but that they should be chainable, to solve complex tasks. There are commands for searching, filtering and modifying text, commands that retrieve information from the OS, or communicate through the network.\\

All aspects of a *nix system can be managed through the command line interface, and servers are typically only accessible through their text interfaces. Because of the low resource usage of a text interface, remote systems are responsive and react to commands quickly.\\

\subsection{MS-DOS}

The Microsoft Disk Operating System was introduced in 1981, it was the first OS developed in part by Microsoft. DOS and its derivatives were text-only OSes without a GUI. Compared to *nix the available commands were simple, and the CLI lacked many of the features offered by unix-like systems.

\subsection{PowerShell}

PowerShell is a task automation and configuration management framework from Microsoft, consisting of a command-line shell and the associated scripting language. Powershell can be used to manage and automate Windows and other Microsoft offerings, such as various services on Azure, Microsoft's cloud platform. It is a powerful and extensible command interpreter with many features.

\section{Bash basics}

If you use modern technologies, like Docker and Continuous Integration/Continuous Delivery, you will come across shell scripting, even if you don't use a shell regularly. I will list some basics in this section, that will help you to navigate a CLI. I will use bash as an example, which is what you are most likely to encounter.\\

Commands in bash are case-sensitive, so \cli{Pwd} and \cli{pwd} do not mean the same thing. Names of files and directories are also case-sensitive, keep this in mind when entering commands.

\subsection{The prompt}
A default bash prompt looks like this: \cli{[user@host ~]\$ }. The prompt is where you enter commands, with the output displayed underneath, on the lines following the prompt. Once the shell is ready for input, it will display the prompt again. Commands are sent to the interpreter with the enter key. The prompt can be customised, but by default it displays the username, the host and the current directory. If your prompt ends in a \cli{\$}, that usually means you are an unprivileged user, and a prompt ending in \cli{\#} means you are a super-user.\\

\subsection{Commands}
Commands are not always useful by themselves, some commands require additional information in the form of arguments, which are separated from the command (and each other) by a space. For example the command \cli{cat readme.txt} prints a file. Here, \cli{cat} is the command, \cli{readme.txt} is the argument. An argument is like a subject in a sentence, with the command being the verb.

\subsection{Basic navigation}
Bash has a concept of a working directory. If not specified otherwise, all commands and file names are interpreted in the context of the working directory. The working directory is by default the home directory of the user. This is usually \cli{/home/username}, and also referred to as \cli{\textasciitilde} (a tilde character).\\

In Linux (and Mac OS), there are no drives, like in Windows. Everything is part of a large directory tree under the root directory, which is denoted by a \cli{/} (slash). The directories in a path are separated by slashes. So \cli{/home/andras} means the \cli{andras} subdirectory under the \cli{home} directory.\\

If the prompt is not configured to show the current directory, you can check it with the \cli{pwd} (print working directory) command.\\

The command \cli{ls} (list) lets you see the contents of the current directory, or a directory that is specified as an argument.\\

You can change the working directory with the \cli{cd} (change directory) command. The argument must be a directory, specified in either relative or absolute format.\\

\begin{quote}
\subsubsection{Relative or Absolute?}
When a directory of file name starts with a slash, then it is understood by commands to be in absolute format, meaning the whole path to the file in the filesystem is specified. If the directory or file name doesn't start with a slash, then it is assumed to be relative to the current working directory.\\
\end{quote}

Each directory has two special entries, \cli{.} and \cli{..} these refer to the current directory and the parent directory, respectively.\\ As an example, if your current directory is \cli{/home/andras/example}, the command \cli{cd ..} will take you to the \cli{/home/andras} directory.\\ The \cli{.} entry can be used to explicitly refer to the current directory. This is useful when you want to run a script in the current directory, for example.

\subsection{Directories}

The commands \cli{mkdir} and \cli{rmdir} can be used to create and delete directories respectively. \cli{rmdir} only works on directories that are empty. If you want to delete a directory which has files in it, then you must use \cli{rm -rf directoryname}. This is a very dangerous command, as it will delete \textbf{everything} in and under the specified directory, without asking.

\subsection{File handling}

You can create a new file with the \cli{touch} command. The command \cli{touch test.txt} will create an empty file named \cli{test.txt} in the current working directory. If you use the \cli{touch} command on an existing file, it will simply update it's modification time.\\

You can copy files with the \cli{cp} (copy) command. For example: \cli{cp test.txt test2.txt} copies the contents of the file \cli{test.txt} into \cli{test2.txt}.\\

The \cli{mv} (move) command can be used to move or rename files and directories. The command \cli{mv /home/andras/example/test.txt /home/andras} moves the \cli{test.txt} file from the \cli{example} subdirectory the home directory of the andras user.\\

Finally, the \cli{rm} (remove) command can be used to delete files. Bash treats its users as adults, so there is no ``Do you really want to delete this?'' confirmation before files are completely deleted. Files deleted with \cli{rm} are not copied to a trash bin, they are really irrevocably deleted. \cli{rm} doesn't delete directories however, deletion of directories can be forced with the \cli{-f} option. The command \cli{rm test.txt} deletes the file \cli{test.txt} for example.

\subsection{Working with the Content of Files}

The command \cli{cat} (concatenate) can be used to print the contents of files. If given a glob or list of files, \cli{cat} prints them after one another, thus concatenating them.\\

\begin{quote}
\subsubsection{Redirecting Output}
The characters \cli{>} and \cli{>>} can be used to redirect the output of a command into a file. A single \cli{>} erases the previous content of the file, while \cli{>>} appends to it. The command \cli{cat test1.txt test2.txt > test3.txt} can be used to store the contents of \cli{test1.txt} and \cli{test2.txt} into \cli{test3.txt}.\\

\subsubsection{Piping Output}
The pipe character \cli{|} lets plug programs together like pipes. The output of the first program will be used as input for the second. It is common to list some infomration with a program, and then search for something in its output with grep: \cli{lsof -P | grep LISTEN} This command will list all processes that have network ports open.\\

\end{quote}

\cli{echo} lets you print things to the standard output. This is useful for looking at the content of variables, for example. The command \cli{echo \${HOME}} will print the path to your home directory, for instance.\\

If you cat a large file, it will scroll past quickly, and you won't be able to read it. A utility that helps in this case is \cli{less}, which lets you scroll the file with the arrow-, or PgUp/PgDn keys. Usage: \cli{less logfile.log} or \cli{cat logfile.log | less}.\\

One of the most useful commands for system administration and troubleshooting is \cli{grep} (globally search for a regular expression and print matching lines). \cli{grep} lets you search for things in files or the output of other programs. A common command is \cli{grep -i error somelogfile.log} when you are troubleshooting. You can also use regular expressions to look for matching patterns in files. Regular expressions enable \cli{grep} to look for patterns instead of specific strings, this makes \cli{grep} very powerful. To `grep' is officially a verb since 2003, and you will hear people saying things like ``I grepped the logs, and found nothing''.\\

Two other commands that let you manipulate are \cli{awk} (Aho, Weinberger and Kernighan, the authors) and \cli{sed} (stream editor). \cli{awk} is a mini-language that lets you manipulate/format textual data and even do maths. \cli{sed} has similar functionality, it is used to make modifications to streams of data, but can also work on files. Both of them can be used to do a lot of things and mastering them takes a lot of time, but they are worth it.

\subsection{Getting Help}

Most commands have a lot of parameters, and keeping them all in your head is simply impossible. Linux has good utilities for accessing and finding documentation.\\

Using the command \cli{man}, you can access so called `manpages', the manuals to all text-based commands on the system. If you enter the command \cli{man ls}, you get the manual for the \cli{ls} command, with an explanation of the parameters, usage examples, and related commands.

If you are looking for a command, but don't know the name, the command \cli{apropos} or \cli{man -k} let you search for commands that have to do with a certain area. For example, if you are looking for command that let you access documentation, you need to enter \cli{apropos documentation} or \cli{man -k documentation}. This will output a list of commands with their descriptions.\\

Another command that is useful, is \cli{info}. Info is more like a book, instead of simple pages, you can navigate it with your keyboard. Pressing `h' in info shows you a handy cheat sheet of what the keys are.

\section{Windows Shell Commands}

A Windows shell is not as full featured as a unix-like shell, but there are simple commands you can use to get around. If you want to manage files on a Windows system, the graphical file managers are far more suited to this. One of the most popular Windows file-managers is Total Commander.\\

Here is a list of basic Windows commands:
\begin{center}
\begin{tabularx}{0.5\textwidth}{ | l | X | }
\hline
\texttt{dir} & List directory contents\\
\hline
\texttt{cd} & Change directory\\
\hline
\texttt{cls} & Clear the screen\\
\hline
\texttt{copy} & Copy files\\
\hline
\texttt{del} & Removes one or more files\\
\hline
\texttt{md} & Make directory\\
\hline
\texttt{move} & Move one or more files\\
\hline
\texttt{rd} & Remove directory\\
\hline
\texttt{ren} & Rename one or more files\\
\hline
\texttt{type} & display contents of a text file\\
\hline
\end{tabularx}
\end{center}

\section{File Hierarchy Standard}

\begin{center}
\begin{tabularx}{\textwidth}{ | l | X | }
\hline
/ & Primary hierarchy root and root directory of the entire file system hierarchy. \\
/bin & Essential command binaries that need to be available in single-user mode, including to bring up the system or repair it,for all users (e.g., cat, ls, cp). \\
/boot & Boot loader files (e.g., kernels, initrd). \\
/dev & Device files (e.g., /dev/null, /dev/disk0, /dev/sda1, /dev/tty, /dev/random). \\
/etc & Host-specific system-wide configuration files. There has been controversy over the meaning of the name itself. In early versions of the UNIX Implementation Document from Bell labs, /etc is referred to as the etcetera directory, as this directory historically held everything that did not belong elsewhere (however, the FHS restricts /etc to static configuration files and may not contain binaries).\\
/home & Users' home directories, containing saved files, personal settings, etc. \\
/lib & Libraries essential for the binaries in /bin and /sbin. \\
/lib<qual> & Alternate format essential libraries. These are typically used on systems that support more than one executable code format, such as systems supporting 32-bit and 64-bit versions of an instruction set. Such directories are optional, but if they exist, they have some requirements. \\
/media & Mount points for removable media such as CD-ROMs (appeared in FHS-2.3 in 2004). \\
/mnt & Temporarily mounted filesystems. \\
/opt & Optional application software packages.\\
/proc & Virtual filesystem providing process and kernel information as files. In Linux, corresponds to a procfs mount. Generally, automatically generated and populated by the system, on the fly. \\
/root & Home directory for the root user. \\
/run & Run-time variable data: Information about the running system since last boot, e.g., currently logged-in users and running daemons. Files under this directory must be either removed or truncated at the beginning of the boot process, but this is not necessary on systems that provide this directory as a temporary filesystem (tmpfs). \\
/sbin & Essential system binaries (e.g., fsck, init, route). \\
/srv & Site-specific data served by this system, such as data and scripts for web servers, data offered by FTP servers, and repositories for version control systems (appeared in FHS-2.3 in 2004). \\
/sys & Contains information about devices, drivers, and some kernel features.\\
/tmp & Directory for temporary files (see also /var/tmp). Often not preserved between system reboots and may be severely size-restricted. \\
/usr & Secondary hierarchy for read-only user data; contains the majority of (multi-)user utilities and applications. Should be shareable and read-only.\\
/var & Variable files: files whose content is expected to continually change during normal operation of the system, such as logs, spool files, and temporary e-mail files. \\
\hline
\end{tabularx}
\end{center}

\section{Useful Links}
\begin{itemize}
\item \href{https://ubuntu.com/tutorials/command-line-for-beginners#1-overview}{The Linux command line for beginners tutorial}
\item \href{https://www.regular-expressions.info/}{A very good site about regular expressions}
\item \href{https://www.computerhope.com/overview.htm}{A good overview of MS-DOS/Windows Command Shell commands}
\end{itemize}

\section{Glossary}
\begin{itemize}
  \item \textbf{GUI} -- Graphical User Interface
  \item \textbf{Command line/Shell/Console} -- These terms are used (interchangibly) to refer to text based environments where commands are typed out to carry out work.
  \item \textbf{Case-sensitive} -- When a system/shell is case-sensitive, it treats upper case and lower case characters as unequal. For example, on a Linux system the filename \texttt{File.txt} refers to a different file than \texttt{file.txt}. There is no difference on Windows (as it is not case-sensitive), the two names would refer to the same file.
  \item \textbf{Filesystem Path} -- The whole absolute path to the file from the root of the filesystem. For example, on Linux, sections of the path are separated by slashes: \texttt{/var/log/sar/sar01} is the full path to a file (a log of metrics, in this case). On Windows, the path always starts with the drive letter, and the sections of the path are separated by backslashes: \texttt{C:\textbackslash Windows\textbackslash System32\textbackslash drivers\textbackslash etc\textbackslash hosts}
\end{itemize}

\section{Exam Questions}
\begin{itemize}
  \item \textbf{When was the first computer mouse invented?} -- 1963
  \item \textbf{What character is used to divide sections of a file path on unix-like systems?} -- The slash character: \texttt{/}
  \item \textbf{If I executed the commands \texttt{echo 'line one' > exam.txt} and \texttt{echo 'line two' > exam.txt} in a Bash shell after each other, without creating the file first, what would the contents of the file \texttt{exam.txt} look like?} -- The contents of \texttt{exam.txt} would be: \texttt{line two}. This is because the simple redirection erases the previous contents of the file. If I had used a double redirection character \texttt{>>} (for appending), the previous lines would have been preserved. Redirection also creates the file, if a file with that name didn't yet exist.
  \item \textbf{What does the Bash command \texttt{rm} do with a directory that has files in it?} -- Nothing, a simple \texttt{rm} doesn't delete directories, even when they are empty.
\end{itemize}

\end{document}