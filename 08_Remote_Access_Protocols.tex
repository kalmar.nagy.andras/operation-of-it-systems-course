\documentclass[12pt]{report}

\newcommand\lessontitle{Lecture 8 -- Remote Access Protocols}
\newcommand\lessondate{March 25, 2021}

\usepackage[a4paper, landscape, margin=2cm]{geometry}
\usepackage{fontspec}
\usepackage{microtype}
\usepackage{fancyhdr}
\usepackage{sectsty}
\usepackage{graphicx}
\usepackage{flafter}
\usepackage{hyperref}
\usepackage{afterpage}
\usepackage{wrapfig}
\usepackage{xcolor}
\usepackage{tabularx}
\usepackage{amssymb}

\graphicspath{ {./images/} }

\setmainfont{CallunaSans-Regular.otf}[
  BoldFont = CallunaSans-Bold.otf ]
\setsansfont{MuseoSans-500.otf}[
  BoldFont = MuseoSans-900.otf,
  ItalicFont = MuseoSans-500Italic.otf ]
\setmonofont{UbuntuMono-R.ttf}

\pagestyle{fancy}
\fancyhf{}
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0.5pt}
\lhead{\textsf{Operation of IT Systems -- ENPTIA2401-PRACT}}
\rhead{\textsf{\lessontitle}}
\rfoot{\textsf{Page \thepage}}
\lfoot{\textsf{\lessondate}}

\setcounter{secnumdepth}{0}

\setlength{\parindent}{0pt}

\sectionfont{\clearpage\Huge\sffamily}
\subsectionfont{\sffamily}

\newcommand\cli[1]{\colorbox{gray!30}{\texttt{#1}}}

\hypersetup{
    colorlinks=true,
    linkcolor=black,
    urlcolor=black,
}
\urlstyle{same}

\begin{document}

\begin{titlepage}
{\Huge \bfseries \textsf{Operation of IT Systems -- ENPTIA2401-PRACT} \par}
\vspace{1cm}
{\Large \bfseries \textsf{\lessontitle} \par}
\vspace{1cm}
{\Large \textsf{\lessondate} \par}
\vfill
{\large \textsf{András Kalmár-Nagy (\href{mailto:andras.kalmar-nagy@t-systems.com}{andras.kalmar-nagy@t-systems.com})}}
\end{titlepage}

\section{A Little History}

\begin{wrapfigure}{r}{0.45\textwidth}
  \begin{center}
    \includegraphics[width=0.43\textwidth]{teletype}
  \end{center}
  \caption{Teletype teleprinters in WWII}
\end{wrapfigure}

Ever since computers were invented, there has been a need to access them remotely. The first computers were very large and physical access to them was limited, but their resources could be made efficiently accessible through remote terminals, so remote access protocols were developed.

\subsection{Teleprinters}

Teleprinters were used as early as 1887 to communicate in telegraphy. A teleprinter is similar to a typewriter, but the text entered is sent through simple wire-pairs, or telephone lines to a remote device. The messages sent back from the remote device are printed on paper by the teleprinter.\\

\subsection{Time-Sharing Services}

With the proliferation of large mainframes for business and academic use, time-sharing systems became popular. You could sit at a terminal, and share processor time one one of these large systems. Remote access to these systems was also through teletype machines, or terminals connected through telephone modems.\\

The text typed on the terminal was sent to the mainframe, and the output of the mainframe was sent back to the terminal.

\subsection{Thin Clients}

Thin clients are computers that don't have performant hardware, and instead rely on a server to host the programs that they access through a graphical remote access protocol. The proliferation of relatively cheap, performant laptops have mostly obsoleted this technology.

\section{Text-based Protocols}

The first protocols used for remotely accessing another computer system were all text-based, since the technology at the time didn't permit rich graphical interfaces, nor was there enough bandwidth for the connections for graphical interfaces.

\subsection{Telnet}

Telnet is a contraction of ``teletype network''. Telnet is a protocol that has been developed in 1969, along with ARPANET, the formal specification was released in 1973. Telnet lets you create a virtual terminal and send keystrokes over the network to another computer.\\

In the early seventies, almost all users of the then ARPANET came from military or academic backgrounds, and security on the networks was not an issue. Telnet sends all keystrokes unencrypted over the network, and this poses a security risk, since anybody who can listen in on the network traffic, can see these keystrokes. As domestic access to the internet began to spread, the security of the telnet data on the network could not be guaranteed, and telnet was obsoleted by SSH, the Secure SHell protocol.\\

The three problems problems that made using telnet insecure are the following:
\begin{enumerate}
\item \textbf{Lack of Encryption} -- Telnet sends everything unencrypted through the network, and this includes sensitive data, like passwords. Anyone with a network packet analyser can read this unencrypted traffic and maybe use the passwords and logins maliciously.
\item \textbf{Lack of Authentication} -- Telnet has no functionality to authenticate the receiving party in the communication. This allows an attacker to mount a man-in-the-middle attack, where the connection is hijacked or diverted by a third party.
\item \textbf{Vulnerabilites in Server Software} -- Most telnet server software was also developed in an era where security was not a priority, and there were a lot of security holes and vulnerabilities in commonly used servers that could be exploited.
\end{enumerate}

Today, the \textbf{use of telnet is strongly discouraged}, although it still has some uses in troubleshooting network connections. You can, for example, use it to talk to a web server on port 80, and send it HTTP commands. Telnet servers usually listen on port 23.

\clearpage
\subsection{Secure Shell}

The secure shell protocol was developed to replace telnet. It features strong public-key cryptography and host authentication.

\subsubsection{Public-key Cryptography}

Public-key cryptography works by using two keys to encrypt and decrypt data. The key-pair is generated from a large random number and is made up of a private and a public key. The public key can be freely and openly distributed and can only be used to encrypt messages. The private key is never disclosed, and can be used to decipher messages encrypted with the public key. For symmetric encryption to work, you would need to exchange the encryption key in advance over a trusted channel (meeting personally, or through a trusted courier). With public key cryptography, you can calculate a common key using the public key of your communication partner, and your own private key. Since the private keys are never sent over the network, they can't be intercepted, and the communication can never be decrypted by unauthorised third parties.

\subsubsection{SSH Tunneling}

SSH is also very useful, because other protocols can be ``tunnelled'' through an SSH connection. This way, even unsecure protocols can be wrapped in an encrypted layer. A common example of this is SFTP, which is the File Transfer Protocol, in itself an unencrypted protocol, passed through an SSH connection. Such tunnels can actually go through several hosts, and they can also be used to reach services deep in protected networks. SSH can also forward graphical X11 desktop data from Linux hosts, which can be used to start windowed graphical applications on remote systems.

\subsection{Powershell Remote Session}

Under Windows, Powershell can be used to create a remote session using a variety of transports. Powershell can use SSH, RPC (Remote Procedure Call), WMI and WS-Management. This provides the ability to remotely administer Windows machines.By using the cmdlets installed with Windows PowerShell, you can establish and configure remote sessions both from the local and remote ends, create customized and restricted sessions, allow users to import commands from a remote session that actually run implicitly on the remote session, configure the security of a remote session, and much more.

\section{Graphical Protocols}


\subsection{Remote Desktop Protocol}

RDP is a Microsoft standard, and was first introduced in 1998. Remote Desktop Protocol allows the server to share a whole desktop or a window to a client. It has some advanced features, where resources on the client machine can be connected to the remote machine. For example:
\begin{itemize}
\item \textbf{Shared clipboard} -- Items on the clipboard are shared between client and server.
\item \textbf{Audio redirection} -- The sound played by the remote system can be redirected to the client.
\item \textbf{File/Drive sharing} -- Files form the client machine are accessible on the remote server.
\item \textbf{Port redirection} -- Ports (and connected devices) of the client system are accessible on the remote system.
\item \textbf{Printer sharing} -- A printer connected to the local system can be used to print from the remote system.
\end{itemize}
RDP supports host authentication and encryption, and does not require additional tunnelling for security.

\subsection{Virtual Network Computing}

The other popular remote desktop technology is VNC. VNC is more popular on unix-like OSes, but is also available for Windows. It is an open standard, and many server and client programs exist. It supports file and clipboard sharing, but none of the more advanced features available for RDP. The underlying Remote Framebuffer Protocol (RFB) is not fully encrypted (passwords are not sent in plaintext, but image data is), and many extensions exist for server and client programs to secure communications. These, however, are not always standard, and cause incompatibilities between different implementations. You can also tunnel VNC through SSH, and this is the most compatible secure option.

\subsection{Independent Computing Architecture}

ICA is a standard developed by the company Citrix Systems. It is used to deliver remote Windows applications to supported clients. Clients exist for Mac OS and Linux too (amongst others), enabling users to access Windows-based software on their clients.

\subsection{Simple Protocol for Independent Computing Environments}

SPICE was developed to serve as a remote access protocol to virtualised systems on the QEMU/KVM hypervisor. It is designed to be available on a large number of devices and even through a browser based-client. This is especially useful in cloud environments.

\section{Glossary}

\begin{itemize}
	\item \textbf{Modem} -- MODulator/DEModulator, a device that could send and receive digital data over an analogue telephone line.
	\item \textbf{Hypervisor} -- Software that runs, and manages virtual machines.
	\item \textbf{Framebuffer} -- An are of memory used to store the image which is sent to the screen. For a remote framebuffer, the image data is sent over the network to the client screen.
\end{itemize}

If you have terms, that you would like explained, drop me an e-mail, and I will include them here.

\section{Exam questions}
\begin{itemize}
  \item \textbf{Why should use of telnet be avoided?} -- The telnet protocol is unencrypted, and sensitive data is broadcast over the network in plain text, allowing anybody to read it.
  \item \textbf{What does SSH stand for?} -- Secure SHell. It is called secure, because it uses encryption to secure the communication on the network between the server and the client.
  \item \textbf{Which is the more advanced protocol, VNC or RDP?} -- RDP, because it allows for more types of resources to be shared between server and client. RDP is also encrypted as standard.
  \item \textbf{Did teletype machines have screens?} -- No, they were networked typewriters, the first ones were invented much earlier than screens.
\end{itemize}

\end{document}

\begin{wrapfigure}{r}{0.5\textwidth}
  \begin{center}
    \includegraphics[width=0.48\textwidth]{IBM_350_RAMAC}
  \end{center}
  \caption{IBM RAMAC 350 disk mechanism}
\end{wrapfigure}
