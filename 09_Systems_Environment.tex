\documentclass[12pt]{report}

\newcommand\lessontitle{Lecture 9 -- Systems Environment}
\newcommand\lessondate{April 1, 2021}

\usepackage[a4paper, landscape, margin=2cm]{geometry}
\usepackage{fontspec}
\usepackage{microtype}
\usepackage{fancyhdr}
\usepackage{sectsty}
\usepackage{graphicx}
\usepackage{flafter}
\usepackage{hyperref}
\usepackage{afterpage}
\usepackage{wrapfig}
\usepackage{xcolor}
\usepackage{tabularx}
\usepackage{amssymb}

\graphicspath{ {./images/} }

\setmainfont{CallunaSans-Regular.otf}[
  BoldFont = CallunaSans-Bold.otf ]
\setsansfont{MuseoSans-500.otf}[
  BoldFont = MuseoSans-900.otf,
  ItalicFont = MuseoSans-500Italic.otf ]
\setmonofont{UbuntuMono-R.ttf}

\pagestyle{fancy}
\fancyhf{}
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0.5pt}
\lhead{\textsf{Operation of IT Systems -- ENPTIA2401-PRACT}}
\rhead{\textsf{\lessontitle}}
\rfoot{\textsf{Page \thepage}}
\lfoot{\textsf{\lessondate}}

\setcounter{secnumdepth}{0}

\setlength{\parindent}{0pt}

\sectionfont{\clearpage\Huge\sffamily}
\subsectionfont{\sffamily}

\newcommand\cli[1]{\colorbox{gray!30}{\texttt{#1}}}

\hypersetup{
    colorlinks=true,
    linkcolor=black,
    urlcolor=black,
}
\urlstyle{same}

\begin{document}

\begin{titlepage}
{\Huge \bfseries \textsf{Operation of IT Systems -- ENPTIA2401-PRACT} \par}
\vspace{1cm}
{\Large \bfseries \textsf{\lessontitle} \par}
\vspace{1cm}
{\Large \textsf{\lessondate} \par}
\vfill
{\large \textsf{András Kalmár-Nagy (\href{mailto:andras.kalmar-nagy@t-systems.com}{andras.kalmar-nagy@t-systems.com})}}
\end{titlepage}

\section{Physical Environment}

\begin{wrapfigure}{r}{0.20\textwidth}
  \begin{center}
    \includegraphics[width=0.17\textwidth]{facebook-rack}
  \end{center}
  \caption{An Opencompute Rack, used at Facebook.}
\end{wrapfigure}

\subsection{Power}

All systems need power to run, this is usually AC mains power. For your laptop, the power brick is usually external, in desktop computers the power supply is contained in the case. In a datacenter, there are either power supplies in the rack mounted servers, or there are dedicated power supply units in the rack that can feed the servers. In datacenters, the power supplies are usually redundant, either on the level of the individual systems, the rack, or per building (or a combination of these).\\

To prevent data loss because of a power outage, you can use an Uninterruptable Power Supply (UPS). UPSes can range from desktop models to large industrial units capable of powering a whole building for a short amount of time. UPSes give you time to either power systems down properly, or to start alternate means of electricity, like generators. Datacenters have large diesel generators that can supply the whole building with power, until the grid mains returns. I have linked a video at the end of the slides, to show you what a generator like this looks like.\\

Datacenter operators usually like to locate datacenters where electricity is cheap, and multiple sources of it are available.

\subsection{Cooling}

Just like your laptop (or even phone), large systems get hot during operation, and cooling them is a very important function. If the processors overheat, then protective throttling mechanisms kick-in to limit the performance of the processor, to protect it from melting itself. This reduces performance, and the service-life of the processor. To prevent this, systems are usually air cooled, and datacenters have elaborate systems to continuously vent clean, cold air through the systems.\\

Cooling consumes the same amount of power as the systems themselves, so half of a datacenter's energy consumption is devoted to cooling.

\clearpage
\subsection{Connectivity}

A server is not much use without connectivity, it is the second most important infrastructure after the servers themselves. Datacenter racks usually have redundant network connections, to prevent connection outages caused by network equipment malfunctions. It is not uncommon for large server systems to have several network interfaces, they can be bundled together to achieve higher throughput, or redundancy in case of failures. Datacenters usually have redundant connections to the internet, that use separate cables and network infrastructure to protect against one of them going down. The main threat to datacenters is usually earthworks, during which cables can be accidentally severed.

\subsection{Maintenance Access}

All systems either malfunction or reach the end of their lives at one point in time. So datacenters need space between the racks for the technicians to be able to access the racked equipment.\\

Routine tasks include installing, removing or swapping a rack module, running power and network cabling and doing upgrades and maintenance on the servers. Because of the cooling, datacenters are extremely noisy environments, and they may also be shielded form magnetic radiation (no service on phones). If you are coordinating physical work on infrastructure from a remote workplace, this can make communicating with the technicians quite tricky.\\

Technicians usually have a small cart that they can roll to any of the racks. This cart contains a screen, a keyboard and a mouse. They can then connect the cart to a server to diagnose failures and setup systems.\\

Some datacenters are are housed in old bunkers or inside large spaces previously used for other industrial purposes. One such datacenter is Pionen in Stockholm, it looks like a scene out of a James Bond movie. Google bought a disused paper mill in Finland, because it has access to cold water for cooling. Some datacenter operator also provide vaults for storing physical backups of data.

\clearpage

\begin{figure}[t]
  \begin{center}
    \includegraphics[width=0.75\textwidth]{pionen}
  \end{center}
  \caption{Pionen Datacenter in Stockholm}
\end{figure}

\section{Service Environment}

Providing an Information and Communications Technology (ICT) service takes a lot of organising. You need to manage technical, process and sales/financial systems.

\subsection{Service management}

IT Service Management (ITSM) provides a framework for running information technology systems as a service to customers.\\

The most commonly used framework is called ITIL. ITIL stands for IT infrastructure Library, and it is a collection of best practices to help manage a service. ITIL defines a couple of large areas that you need to be concerned with: Incident-, Change-, Problem-, and Configuration/Asset Management. It also contains guidance on continuous improvement of the services and service design and implementation.

\subsubsection{Incident Management}

An incident is an unplanned interruption to, or quality reduction of an IT service. The contract between the service provider and a customer defines Service Level Agreements (SLAs). SLAs define how fast incidents should be dealt with, and what kinds of penalties exist when a provider fails to remedy an incident in the given time.\\

Incidents are therefore usually taken very seriously, and most providers have teams dedicated to coordinating incident response with Lead Incident Managers (LIMs). LIMs coordinate the teams that are involved in finding the root cause of an incident and then returning the service to a good operating state.

\subsubsection{Change Management}

It is very unlikely that an IT system will go through its life without any change. ``Never touch a running system'' is an old sysadmin saying, and it shows that changing a functioning system is usually thought of as a dangerous activity. However, changes need to be made to systems, either to upgrade or maintain the hardware underneath the service, or to roll out changes to the software on the systems.\\

When managed correctly, the risks associated with change activities can be minimised, and change management also documents all of the modifications to a system or service through it lifetime. This information is valuable, because it allows administrators to quickly identify changes that may have resulted in an incident. A well managed change completes the activities in the allocated time, and always leaves a system in an operable state, even if the changes need to be rolled back.

\subsubsection{Problem Management}

If a system is frequently involved in incidents, or a change fails, the issues are dealt with by problem managers. A Problem ticket aims to permanently solve a recurring error or problem that degrades the quality of a service. During the problem management process, the root cause of the problem is identified, and a solution is implemented to address the problem. All of the steps are documented in the ticket, and the customer can follow the process through the update to the ticket. Proactive problem tickets are a great way to improve a service and make it more reliable. They are opened by the administrators if they see conditions that can become problems later, for example, when a disk is starting to get full, or a configuration has an error that could lead to an outage.

\subsubsection{Asset Management}

Asset management maintains a list of systems and servicesin a database called the CMDB (Configuration Management DataBase). All of the systems have an netry in the CMDB, and the ticketing system used by the service provider uses this information to connect tickets to systems and administrators, who are responsible for a service.\\

The CMDB contains information about the type and age of systems, what kind of hardware they run on, where they are physically located, what sort of network connections they have, and what kind of software they run. This information can be used to plan maintenance, and hardware or software upgrades.

\subsection{Order Management}

A service provider also needs a way to accept requests from their customers, this is usually handled by a (virtual) service desk that the customer can talk to. The service desk then takes the request information and opens an order request. An order request is usually the basis of a change, and when the change is successful, also provides a means to bill the customer for the work that was performed. An order is usually a complex piece of work, and the ticket is often routed forom team to team as the different task are carried out. For example, the order for a database server provides work for technicians in the datacenter, the networking and backup teams, OS services, application administrators and database administrators and all the people involved in managing the service.

\subsection{Sales/Contract Management}

The sale of new services to customers is the job of the sales department. They also provide billing and feedback on services, and new offerings from the company. They negotiate deals and contracts with customers, and work out proposals with the technical teams.

\section{Interesting Links}
\begin{itemize}
\item \href{https://websitehostreview.com/10-most-incredible-data-centers-on-earth/}{10 Most Incredible Data Centers on Earth}
\item \href{https://www.youtube.com/watch?v=xxcpBz_GMaU}{Flywheel Uninterruptible Power Supply Video}
\item \href{https://www.youtube.com/watch?v=pLBYulLueQo}{Assembling a Server Rack Video}
\end{itemize}

\section{Exam questions}
\begin{itemize}
  \item \textbf{Which is the most commonly used ITSM framework at large companies?} -- ITIL, the IT Infrastructure Library.
  \item \textbf{Why are redundant systems used in datacenters?} -- They provide protection against failures and allow maintenance without downtime.
  \item \textbf{How much energy is used by cooling compared to the system itself?} -- Cooling usually uses as much energy as the system itself, although locating datacenters in cool climates reduces this.
  \item \textbf{What is the job of a UPS?} -- To provide backup power in case of a mains failure until the systems are shut down properly, or alternate means of generating electricity are brought online.
\end{itemize}

\end{document}

\begin{wrapfigure}{r}{0.5\textwidth}
  \begin{center}
    \includegraphics[width=0.48\textwidth]{IBM_350_RAMAC}
  \end{center}
  \caption{IBM RAMAC 350 disk mechanism}
\end{wrapfigure}
