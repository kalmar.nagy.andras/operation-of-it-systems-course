\documentclass[12pt]{report}

\newcommand\lessontitle{Lecture 5 -- Network Configuration}
\newcommand\lessondate{March 4, 2021}

\usepackage[a4paper, landscape, margin=2cm]{geometry}
\usepackage{fontspec}
\usepackage{microtype}
\usepackage{fancyhdr}
\usepackage{sectsty}
\usepackage{graphicx}
\usepackage{flafter}
\usepackage{hyperref}
\usepackage{afterpage}
\usepackage{wrapfig}
\usepackage{xcolor}
\usepackage{tabularx}

\graphicspath{ {./images/} }

\setmainfont{CallunaSans-Regular.otf}[
  BoldFont = CallunaSans-Bold.otf ]
\setsansfont{MuseoSans-500.otf}[
  BoldFont = MuseoSans-900.otf,
  ItalicFont = MuseoSans-500Italic.otf ]
\setmonofont{UbuntuMono-R.ttf}

\pagestyle{fancy}
\fancyhf{}
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0.5pt}
\lhead{\textsf{Operation of IT Systems -- ENPTIA2401-PRACT}}
\rhead{\textsf{\lessontitle}}
\rfoot{\textsf{Page \thepage}}
\lfoot{\textsf{\lessondate}}

\setcounter{secnumdepth}{0}

\setlength{\parindent}{0pt}

\sectionfont{\clearpage\Huge\sffamily}
\subsectionfont{\sffamily}

\newcommand\cli[1]{\colorbox{gray!30}{\texttt{#1}}}

\hypersetup{
    colorlinks=true,
    linkcolor=black,
    urlcolor=black,
}
\urlstyle{same}

\begin{document}

\begin{titlepage}
{\Huge \bfseries \textsf{Operation of IT Systems -- ENPTIA2401-PRACT} \par}
\vspace{1cm}
{\Large \bfseries \textsf{\lessontitle} \par}
\vspace{1cm}
{\Large \textsf{\lessondate} \par}
\vfill
{\large \textsf{András Kalmár-Nagy (\href{mailto:andras.kalmar-nagy@t-systems.com}{andras.kalmar-nagy@t-systems.com})}}
\end{titlepage}

\section{A little History}

\begin{wrapfigure}{r}{0.5\textwidth}
  \begin{center}
    \includegraphics[width=0.48\textwidth]{arpanet}
  \end{center}
  \caption{ARPANET logical map, March 1977}
\end{wrapfigure}

Ever since computers were invented, there has been a need to connect them together, so that resources can be pooled and extended. At first, these connections were in the same room, then the same building, then over a campus or a site. The first wide-area networks were military systems, linking radars and other military sites in the fifties. In the early 1960's, time-sharing systems were built, where users could access large machines through teletype terminals, and run programs. The programs then shared available time on the computer, and were executed in an interleaved fashion. The first time-sharing systems were developed at US universities, to allow their students to use their mainframes. The teletype terminals were connected to the mainframes via telephone lines and modems.\\

In 1966, the  Advanced Research Projects Agency Network (ARPANET) was established, funded by the  United States Department of Defense. It was a network design, where failure of some nodes or connections would not impact the operation of the whole network, as long as there were still some connections left intact. The network could route around the failed parts automatically. At the height of the cold-war, this was intended to make the network resilient to nuclear attacks. The first computers were connected in 1969, and the network was developed quite rapidly from then on. ARPANET was the forefather of today's internet, and many technologies in use today were developed for it.

\section{Networks}

\begin{wrapfigure}{r}{0.45\textwidth}
  \begin{center}
    \includegraphics[width=0.43\textwidth]{network_equipment}
  \end{center}
  \caption{Cables connecting network devices}
\end{wrapfigure}

A computer network is made up of computers connected over digital communication lines in order to share resources provided by the nodes in the network. The communication links can have many forms, such as copper based electrical networks, fibre optic networks or wireless links.\\
A computer network is usually made up of network equipment, computers and other devices that are connected to the network. Today, even your toaster, or a light-bulb can be networked to allow control or monitoring of said devices. The devices connected to the network can provide services such as storage, compute and messaging. Specialised hardware, such as printers or other industrial equipment can also be connected.

\subsection{Packet Based Switching}
Older telephone systems used a system called circuit switching, where a continuous electrical circuit was established between the two telephones involved in the conversation. This system doesn't scale well, and could not be used for digital networks. As a solution, packet-based switching was invented.\\ This method breaks the data into small packets, which have a metadata section (usually control information and source-, and target addresses), and a payload section. These packets can then be sent through a network, like one would send letters through the post. The packets might not even take the same route through a network, or arrive out of order, but the networking software on the device can assemble them in the correct order using the metadata.

\subsection{Switches, Routers and Firewalls}
Connections between parts of the network are made through network devices. Switches simply connect nodes in a local part of the network and pass packets along, while routers can also make intelligent decisions on where to send packets, and usually have more features. Firewalls can be used to restrict connections and enforce security policies. Sometimes all of these three functions are integrated into one consumer device, while the high-performance variants are usually separate rack-mounted devices.

\section{Addressing on the Network}

\subsection{TCP/IP}
To be able to communicate with each other, devices must have a standard way of sending information to each other. These standards are called protocols. One such protocol is TCP/IP, which stands for Transmission Control Program/Internet Protocol. The TCP/IP communication model has four layers, these are the application-, transport-, internet-, and link layers. Each of these layers abstracts away the details of those underneath it. When your computer request a web page from a server, the browser only deals with the application layer. The connection between your computer and the server, that is needed for the browser, is handled on the transport layer. What networks the connection gets routed through, is handled by the internet layer. And in what kind of physical medium the signals actually travel is handled by the link layer. TCP connections are stateful, the parties in the communication acknowledge packets as they arrive, and any lost packets are re-sent.

\subsection{UDP}
User Datagram Protocol (UDP) is another protocol that is commonly used for some services, like Domain Name Resolution and Syslog. UDP is different from TCP in that it is stateless. UDP packets are not guaranteed to arrive. However a UDP connection is simpler to establish, exactly because it doesn't have the overhead that TCP has.

\subsection{Addresses}
Each device on the network has an address, through which it can be found. There are two types of addresses involved in TCP/IP networking. One is the hardware address of the network interface, also called a MAC-address. The second is the IP address of the device. A hardware address is fixed, while an IP address is assigned when the device joins a network. The hardware address is only relevant for the local network the device is on, while the IP address gives a global identifier for the device. IP addresses come in two versions: IPv4 and IPv6. IPv4 addresses were introduced in January 1983. They use 4 bytes to specify an address, and are usually written as four decimal byte values separated by dots. For example, the router on my home network has the address \texttt{192.168.1.2}. Since these are 8 bit values, they are each between 0 and 255.

\subsection{The Problem with IPv4}
Since 32 bits (4 bytes) can only be used to represent 4,294,967,296 different addresses, there is a shortage of IPv4 addresses. Today, there are more than 2 billion computers and 4.88 billion smartphones in the world. They all have at least one connection, and thus need an IP address in order to communicate. So if there are certainly more than 7 billion devices needing an address, how can this still work?

\subsection{The Workarounds: Private networks, NAT and CIDR}

\begin{wrapfigure}{r}{0.5\textwidth}
  \begin{center}
    \includegraphics[width=0.32\textwidth]{nat}
  \end{center}
  \caption{Network Address Translation}
\end{wrapfigure}

There are many techniques that were developed to work around this limitation of IPv4. Large parts of the address space for IPv4 are allocated to private networks, which use TCP/IP, but aren't directly connected to the internet. These networks might have gateways to the internet, but the devices inside the network can have their own IP addresses. This way, two devices in two different private networks can have the same IP address.\\

NAT stands for Network Address Translation, and allows networks to ``hide'' behind a single IPv4 address. Most consumer grade home routers employ NAT to separate their local private networks from the internet. IP addresses in a home network usually use the 192.168.*.* network, but devices on the network appear to have the IP address of the router, when communicating with other devices on the internet. The router relays connections from its local network to the outside network, and keeps track of the connection state, and where each packet from outside must go inside the network.\\

The shortage of IPv4 addresses is also driven by inefficiencies in address range allocation. Large parts of the address ranges are allocated to companies that might not use all of their addresses all the time. Classless Inter-Domain Routing (CIDR) was invented to enable a finer segmentation of networks. CIDR attaches a so-called network mask to the IP address when specifying a network range. The network mask shows how many bits in the address are used as the identifier of the network, and how many are used to identify individual hosts. For example the network in our NAT example would be written as \texttt{192.168.1.0/24}

\subsection{IPv6}

While IPv4 used 32 bits to represent addresses, IPv6 uses 128 bits. This allows for $2^{128}$ addresses. This expands the address space immensely, and provides several benefits. As mentioned, it has a much larger address space, so it does away with all the workarounds that IPv4 had to employ to afford an address for every device. IPv6 addresses should in theory be unique to every device, and globally routable (this, however, poses some privacy risks). It has simplified multicasting, and is easier for routers to process. IPv6 also natively supports Internet Protocol Security (IPsec), a set of built-in security features.

\subsubsection{Transitioning to IPv6}
The transition to IPv6 has been underway for a long time, but as IPv4 and IPv6 are incompatible, it is not an easy step to take. There are technologies, whereby IPv4 networks can transmit IPv6 traffic, and vice-versa, but these are mostly inefficient workarounds. It is possible to transition to an IPv6 network at home, but for most people, the benefits are not worth the extra configuration and performance burden.

\subsection{DHCP}
Dynamic Host Configuration Protocol (DHCP) allows a network interface to get configuration from a server on the network, instead of having to configure it manually. This has several benefits: IP addresses are allocated by a central server, so collisions are eliminated, all devices on the network get the correct configuration, and all of the configuration is kept in one place. If your network supports it, DHCP is an easy way to configure your networking, and you should take advantage of it.

\section{Address and Network Notation, Ports}

\subsection{IPv4}
IPv4 addresses are written using dotted decimal notation, where each of the four bytes is written using the decimal representation of the byte, separated by dots: \texttt{192.168.1.2}\\

IPv4 Networks are usually written the same way, but they also include a netmask. Nowadays the CIDR notation is used. The network for the above address would be written as: \texttt{192.168.1.0/24}. The number after the slash denotes the number of bits used to specify the network, and the remaining bits are used to specify the hosts. In out example, the first 24 bits are the network, so \texttt{192.168.1} and the last 8 bits are used to refer to the hosts inside the network. The network is fixed, and 8 bits gives us 256(-2) addresses for hosts. The first IP in a range is reserved for the gateway, and the last IP is used as a broadcast address, so this is why we must subtract two from the number of addresses. If we combine the two notations, then we can specify the IP address, and the size of the network that it's in: \texttt{192.168.1.2/24}\\

You can choose to have smaller subnets, a /25 subnet only has $2^7-2=126$ addresses available, a /28 only $2^4-2=14$, and so on.

\subsection{IPv6}
IPv6 uses 128 bits to represent an address. This is 16 bytes, four times more that IPv4, and the bytes are grouped into 8 groups of four hexadecimal characters: \texttt{2001:0db8:0000:0000:0000:ff00:0042:8329}.\\

Zeroes on the front of groups can be left off, and consecutive zeroes can be replaced with \texttt{::}. So the above example would look like this in short form: \texttt{2001:db8::ff00:42:8329}.\\

CIDR notation of the networks is the same for IPv6, with the number after the slash denoting how many bits are used to specify the network.

\subsection{Ports}
Each network interface has a number of ports, that allow different services on the device to communicate separately from each other. There are 65536 ports, and software on the device can reserve one or more ports to ``listen'' on. Ports 0-1023 are named ports, and are usually allocated to a standard set of privileged services. The ``high'' ports, 1024-65535, are available for all processes to use. Named ports are usually assigned to application protocols, like HTTP (80/tcp), HTTPS (443/tcp), DNS (53/udp), FTP (21/tcp). The port is attached to the address with a colon: \texttt{192.168.1.2:80} or \texttt{[2001:db8::ff00:42:8329]:80}. For IPv6, brackets are necessary to separate the port colon from the colons used in the address.

\section{Domain Name System}

You've seen that IP addresses are not exactly human friendly. You can memorize IPv4 addresses, if you really want to, but IPv6 addresses are 4 times as long, and more complex. IP addresses also don't contain any human-readable information about the service or host that they refer to.\\

To make machines easier to find, the Domain Name System (DNS) was developed. DNS allows the assignment of human-readable names to IP addresses. The command \texttt{nslookup} can be used to get the IP addresses assigned to a domain name, and vice-versa:

\begin{verbatim}
$ nslookup instagram.com
Non-authoritative answer:
Server:  UnKnown
Address:  192.168.1.2

Name:    instagram.com
Addresses:  2406:da00:ff00::22ea:6c48
          2406:da00:ff00::3401:383b
          2406:da00:ff00::369d:b77d
          2406:da00:ff00::36c6:59a0
          2406:da00:ff00::36cc:8fc4
          2406:da00:ff00::3d6:fdb7
          2406:da00:ff00::3e5:e17d
          2406:da00:ff00::22c2:a958
          35.172.124.40
          52.21.48.38
          54.175.71.14
          54.197.167.243
          54.210.252.14
          3.210.79.117
          3.223.165.89
          23.23.173.152
\end{verbatim}

This lookup show us that the domain name \texttt{instagram.com} can be reached at the following list of addresses. As you can see, Instagram has both IPv4 and IPv6 addresses available. DNS allows more than one IP address to be associated with a name for redundancy.\\

\subsection{Domain Names}
A domain name is made up of the Top-Level Domain (TLD), which is the last part of the name, with the final dot: \texttt{.com, .org, .hu, .co.uk, .io, .museum} and a registered name. Domains cost money to register, and are managed by a so-called domain name registrar. If your choice of domain is not yet taken, you can register a domain for yourself. Subdomains can be added by the domain owner, these usually reference services like: \texttt{www.example.com, ftp.example.com, blog.example.com}. DNS servers are arranged in a hierarchy. DNS always queries the server that is set in your interface configuration, first. If that server cannot resolve the domain, it asks the server above it, and so on, until the request reaches a root-server. There are 13 root-servers worldwide, that have information on all domain names. A local DNS cache can be used to speed up resolution of often-visited domains.\\

The address of your domain name server is usually set by your service provider or provided by DHCP, but can be set to something else manually. Google runs a freely accessible DNS server at the address \texttt{8.8.8.8}, which is quite fast. 

\subsection{Unique Resource Identifiers}
A Unique Resource Identifier (URL) is a string that refers to a resource on the internet. It is made up of the protocol, a domain name or IP address, optionally a port, and a path to the resource.\\ Taking \texttt{https://example.com:8443/images/cat.jpg} as an example, \texttt{https://} is the protocol, Hyper Text Transfer Protocol Secure in this case, \texttt{example.com} is the domain, \texttt{8443} is the port, and \texttt{/images/cat.jpg} is the path. The availability of protocols, ports and paths depends on the target machine. You can also use an IP address instead of a domain name (and yes, example.com resolves to an actual IP address with nslookup): \texttt{https://93.184.216.34:8443/images/cat.jpg}, or \texttt{https://[2606:2800:220:1:248:1893:25c8:1946]:8443/images/cat.jpg}.\\

Domain names make it easier to remember your favourite services, and offer some extra features, like the loadbalancing/failover we saw with the instagram.com domain. If you use domain names, then the domain is resolved in the background by the DNS, and your client actually uses the IP address to talk to the service.

\section{Network configuration for consumer products}

\begin{wrapfigure}{r}{0.5\textwidth}
  \begin{center}
    \includegraphics[width=0.4\textwidth]{windows_network}
  \end{center}
  \caption{Windows network configuration dialog}
\end{wrapfigure}

To configure the network on your computer, you will need some details, or a network with a DHCP server. If configuration is handled via DHCP, then you will have nothing to do, other than selecting the network in Your OS. This can be usually done in the notification area (typically next to the clock on the application/start bar of the OS), or in the settings/control panel are of your OS. If you use a wireless connection to your router, then you need to select the appropriate SSID, and enter the password for it.\\

If you want to set things manually, your are going to need the following values: What IP address you want to set, the netmask in decimal dotted format (more on that later) and the address of your gateway (router, usually). Should you want to, you can also set two DNS Server addresses to use.

\subsection{Netmask in Dotted Decimal Notation}
Remember how we said that with the CIDR notation, you specify how many bits of the 32 bit IPv4 address belong to the network identifier? The dotted notation netmask format is the same. A \texttt{/24} netmask in CIDR notation means that the first 24 bits out of the 32 specify the network. In dotted notation, you can write this in binary as: \texttt{11111111.11111111.11111111.00000000}. If you convert the binary representation of the bytes into decimal, you get \texttt{255.255.255.0}. Another example with a \texttt{/26} network: Binary - \texttt{11111111.11111111.11111111.11000000}, decimal -  \texttt{255.255.255.192}

\section{Network Utilities}
\subsection{ping}
The ping command can be used to check if a host is up and reachable through the network. The name comes from the sound a sonar makes. Ping takes either a domain name or an IP address as an argument:
\begin{verbatim}
$ ping google.com

Pinging google.com [216.58.214.238] with 32 bytes of data:
Reply from 216.58.214.238: bytes=32 time=46ms TTL=118
Reply from 216.58.214.238: bytes=32 time=175ms TTL=118
Reply from 216.58.214.238: bytes=32 time=42ms TTL=118
Reply from 216.58.214.238: bytes=32 time=28ms TTL=118

Ping statistics for 216.58.214.238:
    Packets: Sent = 4, Received = 4, Lost = 0 (0% loss),
Approximate round trip times in milli-seconds:
    Minimum = 28ms, Maximum = 175ms, Average = 72ms
\end{verbatim}
Ping resolves any domain names, and then sends a special packet through the network to the host. The host sends a response back, and the round-trip time in milliseconds is calculated. Ping times are a good indicator of overall link quality, and a low response time is desirable. Some firewalls filter the ICMP packets ping sends, so a negative result does not necessarily mean that the host is unavailable.
\clearpage
\subsection{Traceroute}
Traceroute (sometimes \texttt{tracert}), as the name implies, can be used to trace the path that is used to reach a host on the network. This is useful in debugging unsuccessful connection attempts.
\begin{verbatim}
$ tracert google.com

Tracing route to google.com [216.58.214.238]
over a maximum of 30 hops:

  1     2 ms     2 ms     2 ms  192.168.1.2
  2  2038 ms    36 ms    33 ms  catv-89-133-165-154.catv.broadband.hu [89.133.165.154]
  3   108 ms   414 ms   670 ms  catv-89-135-221-78.catv.broadband.hu [89.135.221.78]
  4    51 ms    23 ms    31 ms  hu-bud04a-rc1-ae-28-2021.aorta.net [84.116.240.178]
  5    57 ms    37 ms    57 ms  hu-bud05a-ri1-ae-5-0.aorta.net [84.116.137.6]
  6    23 ms    46 ms    46 ms  72.14.209.116
  7    75 ms    55 ms    31 ms  74.125.242.241
  8   136 ms    44 ms   385 ms  72.14.237.249
  9    49 ms    36 ms    22 ms  bud02s24-in-f14.1e100.net [216.58.214.238]

Trace complete.
\end{verbatim}
We can see, that to get to Google, our packets must travel through 9 systems altogether. Some of these systems have domain names associated with them, while some are so deep on the backbone, that they don't even have human readable names. A WHOIS service can be used to see who the IPs belong to (Google, in this case).
\clearpage
\subsection{Nmap}
Nmap (network mapper) is a network scanning tool. It can be used to discover hosts and open ports on networks.
\begin{verbatim}
$ nmap -A scanme.nmap.org
Starting Nmap 6.47 ( https://nmap.org ) at 2014-12-29 20:02 CET
Nmap scan report for scanme.nmap.org (74.207.244.221)
Host is up (0.16s latency).
Not shown: 997 filtered ports
PORT     STATE SERVICE    VERSION
22/tcp   open  ssh        OpenSSH 5.3p1 Debian 3ubuntu7.1 (Ubuntu Linux; protocol 2.0)
| ssh-hostkey:
|   1024 8d:60:f1:7c:ca:b7:3d:0a:d6:67:54:9d:69:d9:b9:dd (DSA)
|_  2048 79:f8:09:ac:d4:e2:32:42:10:49:d3:bd:20:82:85:ec (RSA)
80/tcp   open  http       Apache httpd 2.2.14 ((Ubuntu))
|_http-title: Go ahead and ScanMe!
9929/tcp open  nping-echo Nping echo
Warning: OSScan results may be unreliable because we could not find at least 1 open and 1 closed port
Device type: general purpose|phone|storage-misc|WAP
Running (JUST GUESSING): Linux 2.6.X|3.X|2.4.X (94%), Netgear RAIDiator 4.X (86%)
OS CPE: cpe:/o:linux:linux_kernel:2.6.38 cpe:/o:linux:linux_kernel:3 cpe:/o:netgear:raidiator:4 cpe:/o:linux:linux_kernel:2.4
Aggressive OS guesses: Linux 2.6.38 (94%), Linux 3.0 (92%), Linux 2.6.32 - 3.0 (91%), Linux 2.6.18 (91%), Linux 2.6.39 (90%), Linux 2.6.32 - 2.6.39 (90%), Linux 2.6.38 - 3.0 (90%), Linux 2.6.38 - 2.6.39 (89%), Linux 2.6.35 (88%), Linux 2.6.37 (88%)
No exact OS matches for host (test conditions non-ideal).
Network Distance: 13 hops
Service Info: OS: Linux; CPE: cpe:/o:linux:linux_kernel
... (output redacted for brevity)
\end{verbatim}
Nmap tries to connect to each port on the target machine, to see if there is a service listening. If it finds an open port, it tries to guess the service based on the response of the service. Nmap can be used to determine if a port is closed or just blocked by a firewall. It is valuable in diagnosing connection issues.\\
Nmap must be used carefully, and only with the knowledge of the system's admins, as a port scan will be flagged as suspicious activity. Ports scans can potentially mean reconnaissance for a hacking attempt, and can get you into trouble on strictly controlled networks.

\section{Glossary}

\begin{itemize}
	\item \textbf{Mainframe} -- Usually a large computer, or computer cluster, typically in an academic or industrial setting.
	\item \textbf{Modem} -- MOdulator/DEModulator, a piece of hardware that can translate digital signals into analogue signals (and back) for transmission over a telephone line.
	\item \textbf{Ethernet} -- A physical media standard, used to connect networking components. It uses 8 wires to transmit digital signals. There are different categories that represent different speeds, CAT5 is common for 100Mbit/sec network links, while CAT5E and CAT6 allow speeds of over 1Gbit/sec. Your home networking equipment uses this.
	\item \textbf{ADSL} -- Asymmetric Digital Subscriber Line -- A digital link that uses the analogue telephony network as a medium. Download speeds are higher than upload speeds, hence the name.
	\item \textbf{Broadband} -- Digital links that offer speeds over 25-50Mbit/sec, usually television cable or fibre networks.
\end{itemize}

If you have terms, that you would like explained, drop me an e-mail, and I will include them here.

\section{Exam questions}
\begin{itemize}
  \item \textbf{How many hosts can you have in an IPv4 \texttt{/24} network?} -- If we subtract 24 from 32 bits, we are left with 8 bits: $2^8=256$ and we need to subtract 2 for the gateway and the broadcast address. This leaves 254 possible addresses for hosts.
  \item \textbf{How can you connect to a particular service on a host, if you know the address?} -- Services listen on ports, to connect to a specific service, you need to specify a port number with the address.
  \item \textbf{How many bits does IPv4 use for addresses?} -- 32
  \item \textbf{What is DHCP?} -- Dynamic Host Configuration Protocol
\end{itemize}

\end{document}