\documentclass[12pt]{report}

\newcommand\lessontitle{Lecture 4 -- Filesystems}
\newcommand\lessondate{February 25, 2021}

\usepackage[a4paper, landscape, margin=2cm]{geometry}
\usepackage{fontspec}
\usepackage{microtype}
\usepackage{fancyhdr}
\usepackage{sectsty}
\usepackage{graphicx}
\usepackage{flafter}
\usepackage{hyperref}
\usepackage{afterpage}
\usepackage{wrapfig}
\usepackage{xcolor}
\usepackage{tabularx}

\graphicspath{ {./images/} }

\setmainfont{CallunaSans-Regular.otf}[
  BoldFont = CallunaSans-Bold.otf ]
\setsansfont{MuseoSans-500.otf}[
  BoldFont = MuseoSans-900.otf,
  ItalicFont = MuseoSans-500Italic.otf ]
\setmonofont{UbuntuMono-R.ttf}

\pagestyle{fancy}
\fancyhf{}
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0.5pt}
\lhead{\textsf{Operation of IT Systems -- ENPTIA2401-PRACT}}
\rhead{\textsf{\lessontitle}}
\rfoot{\textsf{Page \thepage}}
\lfoot{\textsf{\lessondate}}

\setcounter{secnumdepth}{0}

\setlength{\parindent}{0pt}

\sectionfont{\clearpage\Huge\sffamily}
\subsectionfont{\sffamily}

\newcommand\cli[1]{\colorbox{gray!30}{\texttt{#1}}}

\hypersetup{
    colorlinks=true,
    linkcolor=black,
    urlcolor=black,
}
\urlstyle{same}

\begin{document}

\begin{titlepage}
{\Huge \bfseries \textsf{Operation of IT Systems -- ENPTIA2401-PRACT} \par}
\vspace{1cm}
{\Large \bfseries \textsf{\lessontitle} \par}
\vspace{1cm}
{\Large \textsf{\lessondate} \par}
\vfill
{\large \textsf{András Kalmár-Nagy (\href{mailto:andras.kalmar-nagy@t-systems.com}{andras.kalmar-nagy@t-systems.com})}}
\end{titlepage}

\section{Filesystems}

A file system or filesystem (often abbreviated to fs) controls how data is stored and retrieved. Without a file system, data placed in a storage medium would be one large body of data with no way to tell where one piece of data stops and the next begins. By separating the data into pieces and giving each piece a name, the data is easily isolated and identified. Taking its name from the way paper-based data management system is named, each group of data is called a "file." The structure and logic rules used to manage the groups of data and their names is called a "file system."\\

\subsection{Storage devices}

\begin{wrapfigure}{r}{0.5\textwidth}
  \begin{center}
    \includegraphics[width=0.48\textwidth]{Forty_years_of_Removable_Storage}
  \end{center}
  \caption{Various pieces of removable media}
\end{wrapfigure}

A storage device is a piece of hardware that can contain data. Storage devices are usually optimized for a specific purpose, depending on whether speed, capacity or portability is the priority. We differentiate them based on how they are attached. A storage device can be:

\begin{itemize}
	\item \textbf{Local} -- Local storage devices are usually fixed hardware items in the chassis of the machine.
	\item \textbf{Removable} -- Removable devices are either attached on an external port, like a USB drive, or feature removable media, like an optical or tape drive.
	\item \textbf{Network} -- Network drives are either accessible through a TCP/IP LAN, in which case they are referred to as Network Attached Storage (NAS). Or can have a dedicated Fibre network through a Host Bus Adapter (HBA), in which case they are referred to as a Storage Area Network (SAN). A NAS is usually slower, but doesn't require extra hardware, while a SAN is usually very fast, but expensive.
\end{itemize}

\clearpage
\subsection{Units of Data}

In binary memory, a single \texttt{1} or \texttt{0} value is called a \textbf{bit}. A bit can have two states, either it is \texttt{1} or \texttt{0}. A \textbf{byte} is made up of 8 bits. A byte can have $2^8=256$ states.\\

Apart from being expressed in binary or decimal notation, you might come across a byte written as an octal (prefixed with \texttt{0o}) or hexadecimal value (\texttt{0x}).\\

\begin{center}
\begin{tabularx}{0.5\textwidth} { | >{\centering\arraybackslash}X | >{\centering\arraybackslash}X | >{\centering\arraybackslash}X | >{\centering\arraybackslash}X |}
\hline
Binary & Decimal & Octal & Hexadecimal \\
\hline
\texttt{10110010} & \texttt{178} & \texttt{0o262} & \texttt{0xB2} \\
\hline
\end{tabularx}
\end{center}

Bytes are not multiplied by 1000 to get a kilobyte, but 1024, because $2^{10}=1024$. Therefore:\\

\begin{center}
$1\ byte * 1024 = 1\ Kilobyte * 1024 = 1\ Megabyte * 1024 = 1\ Gigabyte * 1024 = 1\ Terabyte * 1024 = 1\ Petabyte * 1024 = 1\ Exabyte$
\end{center}

Some manufacturers argue that since the SI metric system uses 1000 as the kilo prefix, a kilobyte should be 1000 bytes, and invented the kibibyte for the 1024 conversion. When units are referred to with a capital letter, like K, or KiB, this means the 1024 conversion. And kB, with a small letter k, refers to the 1000 conversion. This allows storage manufacturers to claim their devices are higher capacity, when in fact they are not. Something to look out for, when shopping for storage.

\section{Volume management}

A piece of attached storage is usually referred to as a volume or drive. Windows labels drives with a capital letter (like \texttt{C:}), while unix-like systems refer to them as volumes, that are mounted as a directory in the filesystem.\\

Logical volume managers allow the grouping of physical devices into storage pools or logical volumes. For example, two 5 Gigabyte hard disks and a 10 Gigabyte hard disk could be grouped and made to appear as a 20G logical disk. Disks can also be partitioned, to separate a larger disk into smaller parts.t\\

Volumes can also have different filesystems on the same device. This is relevant, as there are filesystems which are not bootable. In this case, the bootable partition can have a filesystem that was suitable for booting the OS, while another (usually larger) partition can be created for data.

\begin{figure}[b]
	\includegraphics{storage_layers}
	\caption{Layers in a storage system}
\end{figure}

\section{Filesystem features}

Modern filesystems usually organise storage into blocks of bytes (a common size is 4096 bytes or 4K). A file can span several blocks. Some filesystems have advanced features that make working with them easier and safer.\\

Here is an explanation of file-level features:
\begin{itemize}
	\item \textbf{Hard links} -- Hard links allow a filesystem to refer to the same blocks of data with more than one name. The blocks themselves are not duplicated. Hard linking is usually confined to the same logical volume.
	\item \textbf{Soft links} -- Soft links allow files to contain a reference to another file, even across filesystems or volumes.
	\item \textbf{Block journaling} -- Journaling filesystems keep track of data not yet written to the filesystem in a so-called journal.In the event of a power failure, the journal can be used to reconstruct the unwritten changes, and data loss can be minimized or avoided.
	\item \textbf{Metadata journaling} -- Some journaling filesystems only keep track of the metadata, and not the block level data in the journal. This solution trades performance for data safety/integrity.
	\item \textbf{File change log} -- Some filesystems log which files and what metadata changes on the filesystem. This is useful for generating diffs for use by backup systems and auditing filesystem activity.
\end{itemize}

Block-level features:
\begin{itemize}
	\item \textbf{Snapshots} -- Snapshots allow the user to save a state of the filesystem, and to later return to that state. This makes rolling back changes after complex operations very easy at the cost of performance and capacity.
	\item \textbf{Encryption} -- Either filesystem or file based encryption allow the user to encrypt data-at-rest, so that unauthorised parties cannot use the data on the volume, even if they have physical access to the hardware itself.
	\item \textbf{Deduplication} -- Deduplication enables the FS to store repeating data only once, saving capacity.
	\item \textbf{Checksums} -- A checksum is usually a small control value calculated from a block, that is stored in the metadata. If the stored checksum and the value calculated at retrieval don't match, then data corruption has occurred, and action can be taken to recover it.
	\item \textbf{Caches/Buffers} -- Various levels of caching can speed up retrieval of often-read blocks, and write buffering/batching can improve write performance.
	\item \textbf{Multiple devices} -- Various levels of RAID can enable data redundancy or improve IO speeds.
	\item \textbf{Compression} -- Compression can enable a volume to store more data than it has capacity for, at the expense of increased processing requirements and degraded IO performance.
\end{itemize}

Metadata features:
\begin{itemize}
	\item \textbf{File ownership} -- The filesystem has a concept of file ownership. This is usually to support multi-user OSes.
	\item \textbf{File permissions} -- Read/write/execute permissions can be set separately on files, with different levels based on user group membership.
	\item \textbf{Timestamps} -- Timestamps for creation/access/modification are stored for files.
	\item \textbf{Access control lists} -- ACLs allow even finer grained permissions to be set on files than plain file permissions. ACLs are user and file specific.
	\item \textbf{Security/MAC labels} -- Mandatory Access Control labelling prevents specific processes running on the machine from accessing files.
	\item \textbf{Checksums} -- As with block-level checksums, this feature monitors data integrity.
	\item \textbf{Quotas} -- Quotas allow an OS administrator to set limits on usage for users of a filesystem. This prevents users from (accidentally) filling a volume and denying other users capacity.
\end{itemize}

Modern filesystems allow the user to resize them as more capacity is added, and this operation can be carried out online, on a running machine. It is fairly common for grow operations to be online, and some filesystems also support online shrinking of partitions. Older filesystems, because of memory or other, often physical limitations, had quite restrictive limits on the maximum size of the FS or individual files. Limits for newer filesystems are so large, that encountering them is virtually impossible (on the magnitude of several Exabytes).\\

There are also special filesystems that are created in RAM or represent file-like interfaces to system information. On unix-like systems, \textbf{tmpfs} can be used to create a filesystem in RAM that can then be used like a normal filesystem. The advantage of a RAM based filesystem is speed, while its main drawback is its temporary nature. Data on a tmpfs filesystem is not permanent, and is destroyed when the machine is shut down. The Linux kernel exposes a set of special filesystems called \textbf{procfs} and \textbf{sysfs} where system and hardware information is represented as files and can be read with normal commands like \texttt{cat}. Some files in sysfs can also be used to write information and settings to hardware devices.

\section{Common filesystems}

\subsection{Apple}

Apple File System (APFS) is a proprietary file system for macOS High Sierra (10.13) and later, iOS 10.3 and later, tvOS 10.2 and later, watchOS 3.2 and later, and all versions of iPadOS. It supports encryption, snapshots, clones, metadata checksums and compression amongst other features. It replaced the older HFS+ filesystem.

\subsection{Microsoft}

NTFS (New Technology File System) is a proprietary journaling file system developed by Microsoft. Starting with Windows NT 3.1, it is the default file system of the Windows NT family, this includes Windows XP through to Windows 10. NTFS supports hard links, compression, alternate data streams, sparse files, quotas, ACLs, encryption and online resizing.

Various versions of the File Allocation Table (FAT) filesystem are in use by Microsoft products, and subsequently other storage media. FAT originated as a filesystem for 8 inch floppy disks. It used 8 bits to store entries in the table. It was later extended to FAT16 and then FAT32. FAT32 only allowed a maximum file size of 4GB which made working with large video files very cumbersome. The current version of FAT is exFAT, which is used as the filesystem for removable media such as USB drives and memory cards.

\subsection{Unix-like systems}

Linux generally uses a filesystem called ext4 (with ext3 and ext2 before that) to create its partitions, although XFS and btrfs are also common. ext4 is generally used in conjunction with the Logical Volume Manager (LVM) to enable advanced volume management.\\

There is also an advanced filesystem, ZFS, which originated from Solaris, another Unix variant, used on Sun Microsystems machines. After the source-code for ZFS was released in 2005, it was ported to other unix-like systems and OpenZFS was born. OpenZFS is an open-source storage platform that encompasses the functionality of traditional filesystems and volume manager. It includes protection against data corruption, support for high storage capacities, efficient data compression, snapshots and copy-on-write clones, continuous integrity checking and automatic repair, encryption, remote replication with ZFS send and receive, and RAID-Z.

\clearpage
\subsection{Cluster filesystems}

Lustre is a type of parallel distributed file system, generally used for large-scale cluster computing. The name Lustre is a portmanteau word derived from Linux and cluster. Lustre provides high performance file systems for computer clusters ranging in size from small workgroup clusters to large-scale, multi-site systems. Since June 2005, Lustre has consistently been used by at least half of the top ten, and more than 60 of the top 100 fastest supercomputers in the world, including the world's No. 1 ranked TOP500 supercomputer in June 2020, Fugaku, as well as previous top supercomputers such as Titan and Sequoia.\\

Lustre file systems are scalable and can be part of multiple computer clusters with tens of thousands of client nodes, tens of petabytes (PB) of storage on hundreds of servers, and more than a terabyte per second (TB/s) of aggregate IO throughput. This makes Lustre file systems a popular choice for businesses with large data centres, including those in industries such as meteorology, simulation, oil and gas, life science, rich media, and finance. The IO performance of Lustre has widespread impact on these applications and has attracted broad attention.

\subsection{Specialized filesystems}

Automatic Storage Management (ASM) is a feature provided by Oracle Corporation within the Oracle Database from release Oracle 10g (revision 1) onwards. ASM aims to simplify the management of database datafiles, control files and log files. To do so, it provides tools to manage file systems and volumes directly inside the database. This allows the database to have more control over the filesystem, improving performance.\\

The InterPlanetary File System (IPFS) is a protocol and peer-to-peer network for storing and sharing data in a distributed file system. IPFS uses content-addressing to uniquely identify each file in a global namespace connecting all computing devices.

IPFS allows users to not only receive, but to host content, in a similar manner to BitTorrent. As opposed to a centrally located server, IPFS is built around a decentralized system of user-operators who hold a portion of the overall data, creating a resilient system of file storage and sharing. Any user in the network can serve a file by its content address, and other peers in the network can find and request that content from any node who has it, using a distributed hash table (DHT).

\section{Glossary}

\begin{itemize}
	\item \textbf{IO (I/O)} -- Abbreviation of Input/Output. Used as a synonym of Read/Write. For example ``IO Performance'' usually means bandwidth of read/write operations.
	\item \textbf{Fibre network} -- A network where data is sent through optical fibre cables. This usually allows for high speed, but equipment is more expensive than for copper networks. Care must be taken with optical fibre, this complicates routing of the cables.
\end{itemize}

\section{Exam questions}
\begin{itemize}
  \item \textbf{What does NAS stand for?} -- Network Attached Storage
  \item \textbf{Can a hard link span filesystems?} -- No
  \item \textbf{How many bits is 4 bytes?} -- 32
  \item \textbf{What benefits does a volume management system give us?} -- Flexibility, increased abstraction, control
\end{itemize}

\end{document}