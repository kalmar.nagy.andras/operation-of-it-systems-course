\documentclass[12pt]{report}

\newcommand\lessontitle{Lecture 7 -- Filesystems and RAID}
\newcommand\lessondate{March 18, 2021}

\usepackage[a4paper, landscape, margin=2cm]{geometry}
\usepackage{fontspec}
\usepackage{microtype}
\usepackage{fancyhdr}
\usepackage{sectsty}
\usepackage{graphicx}
\usepackage{flafter}
\usepackage{hyperref}
\usepackage{afterpage}
\usepackage{wrapfig}
\usepackage{xcolor}
\usepackage{tabularx}
\usepackage{amssymb}

\graphicspath{ {./images/} }

\setmainfont{CallunaSans-Regular.otf}[
  BoldFont = CallunaSans-Bold.otf ]
\setsansfont{MuseoSans-500.otf}[
  BoldFont = MuseoSans-900.otf,
  ItalicFont = MuseoSans-500Italic.otf ]
\setmonofont{UbuntuMono-R.ttf}

\pagestyle{fancy}
\fancyhf{}
\renewcommand{\headrulewidth}{0.5pt}
\renewcommand{\footrulewidth}{0.5pt}
\lhead{\textsf{Operation of IT Systems -- ENPTIA2401-PRACT}}
\rhead{\textsf{\lessontitle}}
\rfoot{\textsf{Page \thepage}}
\lfoot{\textsf{\lessondate}}

\setcounter{secnumdepth}{0}

\setlength{\parindent}{0pt}

\sectionfont{\clearpage\Huge\sffamily}
\subsectionfont{\sffamily}

\newcommand\cli[1]{\colorbox{gray!30}{\texttt{#1}}}

\hypersetup{
    colorlinks=true,
    linkcolor=black,
    urlcolor=black,
}
\urlstyle{same}

\begin{document}

\begin{titlepage}
{\Huge \bfseries \textsf{Operation of IT Systems -- ENPTIA2401-PRACT} \par}
\vspace{1cm}
{\Large \bfseries \textsf{\lessontitle} \par}
\vspace{1cm}
{\Large \textsf{\lessondate} \par}
\vfill
{\large \textsf{András Kalmár-Nagy (\href{mailto:andras.kalmar-nagy@t-systems.com}{andras.kalmar-nagy@t-systems.com})}}
\end{titlepage}

\section{A little History}

\begin{wrapfigure}{r}{0.5\textwidth}
  \begin{center}
    \includegraphics[width=0.48\textwidth]{IBM_350_RAMAC}
  \end{center}
  \caption{IBM RAMAC 350 disk mechanism}
\end{wrapfigure}

So called hard drives were invented by IBM in the fifties. The first harddisk, the IBM 350 disk storage unit was part of the RAMAC 350 system in 1956. The RAMAC 350 weighed 971 kilograms, and the disk unit had a capacity of 5 Megabytes. It had one read/write head, that was moved with servos between the platters. A RAMAC 350 system cost \$34,500USD in 1956 (over \$335,527 in today's money).\\

The first ``consumer'' hard drives started to be available during the eighties, with the proliferation of the IBM PCs. For example, the IBM PC XT came standard with a 10 Megabyte internal hard disk in 1983.\\

The capacity and reliability of these drives was far behind those used in mainframes and large computers. On the other hand, they were also much cheaper. This lead to research in how they could be utilised in disk arrays.\\

The term "RAID" was invented by David Patterson, Garth A. Gibson, and Randy Katz at the University of California, Berkeley in 1987. They described the idea in their June 1988 paper "A Case for Redundant Arrays of Inexpensive Disks (RAID)".

\section{RAID Levels}

\begin{wrapfigure}{r}{0.5\textwidth}
  \begin{center}
    \includegraphics[width=0.48\textwidth]{storage_server}
  \end{center}
  \caption{Rack mounted storage servers, each with 24 drives}
\end{wrapfigure}

Data in a RAID setup is distributed among the disks in the Array. How the data is distributed is determined by the so called RAID Level. A RAID system employs mirroring, striping and parity to create large data stores made up of a large number of ``cheap'' disks. RAID can be used to create drives that have a larger overall capacity than the constituent drives, and/or it can be used to improve the aggregate reliability of the RAID drive. Some RAID levels can be combined to get both benefits. The RAID level number only serves to identify the kind of setup used, a higher RAID level does not indicate better performance or higher reliability.\\

RAID requires specialised hardware (or software) to control and coordinate the disk operations, and these controllers can differ in performance and the RAID levels they support. Sometimes the controller is implemented in software, which then uses the host's CPU to do the calculations required for some of the RAID features.\\

RAID does not guarantee that no data loss can occur, and does not protect against data corruption, damage caused by malicious intent or accidental deletions. It is not a replacement for a proper backup strategy.

\section{RAID 0}

\begin{wrapfigure}{r}{0.3\textwidth}
  \begin{center}
    \includegraphics[width=0.25\textwidth]{RAID_0}
  \end{center}
  \caption{RAID level 0}
\end{wrapfigure}

Level 0 does not improve reliability, but can be used to create a large capacity array. It splits or ``stripes'' the data across the disks, storing blocks of one file on different drives. For example, you can put five 10 Gigabyte disks into a RAID 0 array to form one 50 Gigabyte volume. Since different parts of one file are on different disks, the read and write operations can be performed in parallel on the disks. This can in theory lead to better performance, since the controller doesn't need to wait as much for the drives, as the blocks are not read/written sequentially. One large drawback of the RAID 0 level is that the failure of one drive will lead to data loss, and most files will be unrecoverable, since the data is spread among the drives. RAID 0 requires a minimum of two drives to function, but can be made up of any number of drives.

\section{RAID 1}

\begin{wrapfigure}{r}{0.3\textwidth}
  \begin{center}
    \includegraphics[width=0.25\textwidth]{RAID_1}
  \end{center}
  \caption{RAID level 1}
\end{wrapfigure}

Level 1 creates a mirrored array, with each block written out to two separate disks. This increases reliability, since there are two copies of every block. If one drive fails in the array, the data is still available on a second disk, and can be copied to restore the mirror. Write performance is limited by the performance of the individual disks, but read performance can be better, because different blocks can be read in parallel from the drives. Capacity is determined by the size of the disks making up the array. Because of the redundancy, the sum of the capacity of the drives is halved in a RAID 1 array.

\subsection{RAID 10 and 01}

You can combine levels 0 and 1 to create large volumes of striped and mirrored disks. The difference between 10 and 01 is how the striping and mirroring is done. For example, you can create a 3 drive RAID 0 volume, and then mirror it to 6 drives, this will give you a RAID 01 array. If you take three RAID 1 volumes and stripe data across them, then that will give you a RAID 10 array. RAID 10 is generally preferred, since it is easier to add disks to such a setup.

\subsection{RAID 2, 3 \& 4}

These RAID levels are obsoleted by levels 5 and 6 and are extremely rarely used in practice. RAID 2 and 3 for example, require all disks in the array to spin in a synchronised fashion, a complexity which other levels don't require.

\section{RAID 5}

\begin{wrapfigure}{r}{0.5\textwidth}
  \begin{center}
    \includegraphics[width=0.45\textwidth]{RAID_5}
  \end{center}
  \caption{RAID level 5}
\end{wrapfigure}

Level 5 uses a technique called parity to calculate a sort of checksum of the data, that can be used to calculate the missing data if one drive is lost from the array. This is combined with striping to create an array of 3+n disks. Since this parity data must also be stored, a level 5 array requires at least 3 disks, but can have any number of them. A level 5 array is capable of surviving the loss of one disk. Level 5 is in improvement over level 4 in that the parity information is also striped among the disks. The striping is laid out in such a fashion, that two consecutive parity blocks are not written to the same disk. As the writing of the parity information needs to be done with every write, this distributes the work among the drives equally.\\

The parity information is calculated from the data that is written, therefore it changes with even the slightest modification (which may not affect all of the chunks in the stripe), and needs to be re-written on all write operations. The calculation of the parity data is handled by specialised accelerators in the more expensive RAID controllers, while it is offloaded to the host CPU in cheaper hardware or software controllers. This used to be quite a bottleneck for RAID 5 (and 6) performance, but today's multicore processors can perform better (since they are usually underutilised).

\section{RAID 6}

\begin{wrapfigure}{r}{0.5\textwidth}
  \begin{center}
    \includegraphics[width=0.45\textwidth]{RAID_6}
  \end{center}
  \caption{RAID level 6}
\end{wrapfigure}

RAID level 6 uses two parity block instead of one, as in level 5, and can thus survive the loss of two disks at the same time.\\

This increased fault tolerance is offset by the need to calculate two sets of parity blocks, therefore write performance is the worst of all RAID levels, with small/random writes suffering the most.

\section{Software implementations}

Operating system and filesystem support for RAID levels in software is as follows:\\

\begin{tabularx}{\textwidth}{ |l|l|l|l|l|X| }
  \hline
  OS & \texttt{RAID 0} & \texttt{RAID 1} & \texttt{RAID 5} & \texttt{RAID 6} & combinations, additional features\\
  \hline
  Mac OS  & \checkmark & \checkmark & & & \texttt{1+0}\\
  \hline
  FreeBSD & \checkmark & \checkmark & \checkmark & & all of these \\
  \hline
  Linux & \checkmark & \checkmark & \checkmark & \checkmark & all of these\\
  \hline
  Windows & \checkmark & \checkmark & \checkmark & & \\
  \hline
  \hline
  FS & \texttt{RAID 0} & \texttt{RAID 1} & \texttt{RAID 5} & \texttt{RAID 6} & combinations\\
  \hline
  ZFS & \checkmark & \checkmark & \checkmark & \checkmark & \texttt{1+0, 5+0, 6+0 \& RAID 7}\\
  \hline
  Btrfs & \checkmark & \checkmark & & & \texttt{1+0}\\
  \hline
  GPFS/Spectrum Scale & & \checkmark & & & Metro-scale \texttt{RAID 1}, declustered RAID\\
  \hline
\end{tabularx}

\subsection{Network Storage Arrays}

\begin{wrapfigure}{r}{0.5\textwidth}
  \begin{center}
    \includegraphics[width=0.4\textwidth]{ovh_fire}
  \end{center}
  \caption{OVH Datacenter fire}
\end{wrapfigure}

Exascalers like Facebook, Google and Amazon have adopted a sort of distributed RAID, where 1+n copies of a chunk of data are distributed among large pools of storage nodes, so that failures are minimised and recovery of lost chunks is always possible from the other copies. They use this internally and also as a feature of their cloud storage offerings.\\

Data can be distributed in a region or even globally, to increase resiliency and also to provide copies of the data to serve regionally. For example, a copy of a Youtube video may be present in all Google regions to allow fast streaming, or only in the regions where it is accessed frequently. Similarly, a storage ``bucket'' on Amazon S3 may be replicated only in the datacenter where it is needed by the customer, or in a region or globally, to survive catastrophic failures of a datacenter, or multiple datacenters in a region.


\section{Glossary}

\begin{itemize}
	\item \textbf{Striping} -- Writing the consecutive blocks of a file on consecutive disks in an array.
	\item \textbf{Mirroring} -- Writing the same Block twice on different disks in an array.
	\item \textbf{Parity} -- A data checksum value that allows the reconstruction of a missing block from the remaining blocks and the parity value.
	\item \textbf{RAID Controller} -- Specialised hardware that implements the control logic required for the various RAID levels. Usually also includes an interface to recover and manage the disks in an array.
\end{itemize}

If you have terms, that you would like explained, drop me an e-mail, and I will include them here.

\section{Exam questions}
\begin{itemize}
  \item \textbf{Can you create a RAID array without controller hardware?} -- Yes, software implementations of RAID exist, but they may be limited in their capabilities. The number of disks in the array determines what levels are possible.
  \item \textbf{How many simultaneous drive failures can a RAID 6 array survive?} -- A RAID 6 array will continue to function normally with up to two failed drives.
  \item \textbf{At least how many drives do you need for a RAID 5 array?} -- At least three, because you need at least two drives for an array, and an additional disk to store parity information.
  \item \textbf{Does a mirrored RAID 1 array eliminate the need for backups?} -- No. Data corruption and accidental (or malicious) deletions of data can still occur, and RAID does not protect against these cases of data loss.
\end{itemize}

\end{document}